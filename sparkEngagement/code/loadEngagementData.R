##########################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: read the data
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# updated on : 2019-03-12
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

library(data.table)
library(futile.logger)
library(sparklyr)
library(dplyr)
library(sparkLearning)

loadEngagementData <- function(sc, sparkDBconURL, sparkDBconURL_stage, startHorizon, useForProbability)
{
        
    loadedData <- list()

    # dbGetQuery(con,"SET NAMES utf8")

    # read the interactions table
    flog.info("read Interaction table")
    interactionsSQL <- sprintf("select * from Interaction where startDateLocal>'%s'", startHorizon)
    interactions <- sparkReadFromDB(sc, sparkDBconURL, interactionsSQL, name="interactions", partitionColumn="interactionId")
    interactions <- dplyr::rename(interactions, eventdate = startDateLocal)
      
    # read the interaction account table
    flog.info("read InteractionAccount table")
    interactionAccountSQL <- "select interactionId, accountId from InteractionAccount"
    interactionAccount <- sparkReadFromDB(sc, sparkDBconURL, interactionAccountSQL, name="interactionAccount", partitionColumn="interactionId")

    # merge with the interactions
    interactions <- interactions %>% inner_join(interactionAccount, by="interactionId")

    # read the account table
    flog.info("read Account table")
    accounts <- sparkReadFromDB(sc, sparkDBconURL, "select accountId, externalId from Account", name="accounts", partitionColumn="accountId")

    accounts <- dplyr::rename(accounts, accountUID = "externalId")

    # read the rep table
    flog.info("read Rep table")
    reps <- sparkReadFromDB(sc, sparkDBconURL, "select repId, externalId from Rep", name="reps", partitionColumn="repId")

    reps <- dplyr::rename(reps, repUID = "externalId")

    flog.info("read repAssignments table")

    repAssignments <- sparkReadFromDB(sc, sparkDBconURL, "SELECT repId, accountId FROM RepAccountAssignment", name="repAssignments", partitionColumn="repId")
    
    # from RepAccountAssignment, get only accounts existing in Rep table
    accts <- repAssignments %>% 
      inner_join(select(reps, repId), by="repId") %>% 
      select(accountId) %>% 
      distinct()

    # filtering interactions using accountId
    interactions <- interactions %>% inner_join(accts, by="accountId")

    # update accounts and reps using RepAccountAssignment
    accounts <- accounts %>% inner_join(accts, by="accountId")

    repIds <- repAssignments %>% select(repId) %>% distinct()

    reps <- reps %>% inner_join(repIds, by="repId")

    # flatten the interactions with merges of accounts and reps
    interactions <- interactions %>% inner_join(accounts, by="accountId")

    interactions <- interactions %>% inner_join(reps, by="repId")

    interactions <- interactions %>% select(accountUID, accountId, repUID, repId, eventdate, repActionTypeId)

    # prepare for merge with the suggestions tables
    interactions <- mutate(interactions, reaction = "Touchpoint") 

    interactions <- dplyr::rename(interactions, date = "eventdate")

    # read the feedback table from the stage db
    flog.info("read stage db Suggestion_Feedback_vod__c_arc table")
    
    feedbackSQL <- "select Id, RecordTypeId, LastModifiedDate, Suggestion_vod__c from Suggestion_Feedback_vod__c_arc"
    feedback <- sparkReadFromDB(sc, sparkDBconURL_stage, feedbackSQL, name="feedback", partitionColumn="Id")
    feedback <- feedback %>% select(-Id) %>% distinct()

    feedback <- dplyr::rename(feedback, feedbackDate = LastModifiedDate)
    feedback <- feedback %>% mutate(feedbackDate = as.character(feedbackDate))

    # read the record types from the stage db
    flog.info("read stage db RecordType_vod__c_arc table")
    recordType <- sparkReadFromDB(sc, sparkDBconURL_stage, "select Id, Name from RecordType_vod__c_arc", name="recordType", partitionColumn="Id")

    recordType <- dplyr::rename(recordType, RecordTypeId = "Id")

    # merge the recordtype into the feedback table to flatten
    feedback <- feedback %>% inner_join(recordType %>% distinct(), by = "RecordTypeId")

    feedback <- dplyr::rename(feedback, reaction = "Name")

    # rename feedback keys
    feedback <- mutate(feedback, reaction = ifelse(reaction=="Activity_Execution_vod", "Execution", reaction))

    feedback <- mutate(feedback, reaction = ifelse(reaction=="Dismiss_vod", "Dismiss", reaction))

    feedback <- mutate(feedback, reaction = ifelse(reaction=="Mark_As_Complete_vod", "Complete", reaction))

    # more cleanup of uneeded fields
    interactions <- interactions %>% select (-c(accountUID, repUID))

    # read the suggestion table from the stage db
    flog.info("read stage db Suggestion_vod__c_arc table")

    table <- "Suggestion_vod__c_arc"
    suggestionsSQL <- sprintf("select Id, OwnerId, Account_vod__c, Record_Type_Name_vod__c, Suggestion_External_Id_vod__c, CreatedDate, Title_vod__c from %s where Call_Objective_Record_Type_vod__c='Suggestion_vod' and Call_Objective_From_Date_vod__c>'%s'", table, startHorizon)
    suggestions <- sparkReadFromDB(sc, sparkDBconURL_stage, suggestionsSQL, name="suggestions", partitionColumn="Id")

    oldnames <- c("Id","OwnerId","Account_vod__c","Record_Type_Name_vod__c","CreatedDate")
    newnames <- c("Suggestion_vod__c","repUID","accountUID","type","date")
    suggestions <- dplyr::rename_at(suggestions, vars(oldnames), ~ newnames) 
                                                                 ############### Kludge - this is to match the keys used in the DSE DB
    suggestions <- suggestions %>% mutate(repActionTypeId = ifelse(type == "Call_vod", 3, NA))
                                                               ############### Kludge - this is to match the keys used in the DSE DB
    suggestions <- suggestions %>% mutate(repActionTypeId = ifelse(type == "Email_vod", 12, repActionTypeId))

    ############### Kludge - this is to match the keys used in the DSE DB
    ##TODO: add web interation type 13
    suggestions <- mutate(suggestions, repActionTypeId = ifelse(type == "Call_vod" & rlike(Suggestion_External_Id_vod__c, "~r[[:digit:]]+~W~"), 13, repActionTypeId))

    suggestions <- mutate(suggestions, date = as.Date(date))

    # merge the suggestions and associated feedback if any
    flog.info("merge suggestions and feedback")

    suggestions <- suggestions %>% left_join(feedback, by="Suggestion_vod__c")

    suggestions <- mutate(suggestions, reaction = ifelse(is.na(reaction), "Ignored", reaction))

    # cleanup uneeded fields
    suggestions <- select(suggestions, -c(Suggestion_vod__c, RecordTypeId))

    # flatten the suggestions table
    suggestions <- suggestions %>% inner_join(accounts, by="accountUID")

    suggestions <- suggestions %>% inner_join(reps, by="repUID")

    # more cleanup of uneeded fields
    suggestions <- suggestions %>% select(-c(accountUID, repUID))
    
    # cache to memory for later faster processing
    flog.info("caching interactions")
    interactions <- sdf_register(interactions, "interactions_after_loadEngagementData")
    tbl_cache(sc, "interactions_after_loadEngagementData")
    interactions <- tbl(sc, "interactions_after_loadEngagementData")
    
    flog.info("caching suggestions")
    suggestions <- sdf_register(suggestions, "suggestions_after_loadEngagementData")
    tbl_cache(sc, "suggestions_after_loadEngagementData")
    suggestions <- tbl(sc, "suggestions_after_loadEngagementData")

    loadedData[["interactions"]] <- interactions
    loadedData[["suggestions"]] <- suggestions

    flog.info("Done with loadEngagementData.")
    
    return(loadedData)
}
