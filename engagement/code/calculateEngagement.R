####################################################################################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: estimate the target engagement probabilities
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

# helper function to estimate the likelihood for a visit based on the ECDF function fn
E <- function(fn,x) return(fn$estimate[which(abs(fn$eval.points-x)==min(abs(fn$eval.points-x)))])

# main entry into the estimation function
calculateEngagement <- function(chl, useForProbability)
{
    library(ks)
    library(data.table)
    library(foreach)
    library(doMC)

    registerDoMC(numberCores)  # setup the number of cores to use
    
    flog.info("calculateEngagement for channel %s ...", chl)

    events <- interactions[repActionTypeId==chl & date<=today]  # subset the interactions for the right channel and today date
    events <- unique(events,by=c("accountId","repId","date"))   # no need to have multiple records with the same account-rep-date combo

      suggestions <- suggestions[repActionTypeId==chl & date<=today & date>=sugStartDate]  # subset the suggestions for channel
    
      setnames(suggestions,c("Suggestion_External_Id_vod__c"),c("suggestion"))
      suggestions$suggestion <- gsub("AKTSUG~","",suggestions$suggestion)                  # remove the prefix AKTSUG~ from the suggestions

      trig <- grep("TRIG",suggestions$suggestion)                                          # identify the Trigger driven suggestions
      if(length(trig)>0){sugs <- suggestions[!(c(1:(dim(suggestions)[1])) %in% trig)]} else {sugs <- suggestions}  # remove them from subsequent analysis

      sugs[,feedbackDate:=as.Date(feedbackDate)]                                           # change field type
      sugs[!(reaction %in% includeReactions),reaction:="Ignored"]                          # if reaction to suggestion is not in parameter includeReactions then set the reaction to IGNORE
      sugs$recordId <- 1:nrow(sugs)                                                        # for subsequent processing number the records in the new suggestion table

      ign <- sugs[reaction=="Ignored"]                                                     # subset the ignored suggestions
      if(nrow(ign)>0)                                                                      # if there are any ignored suggestions than find the time from suggestion to next action
      {
          ign <- rbind(ign,events,fill=T)                                                  # append the ignored suggestions to the intereactions table (now called events)
          setkey(ign,repId,accountId,date)                                                 # sort ign table as setup to find time between suggestion and next action
          ign[,dur:=c(NA,diff(date)),by=c("repId","accountId")]                            # add the dur field to the table that contains that difference in time
          ign[,dur:=shift(dur,type="lead")]                                                # move the dur field values to the rows that contain the suggestion associated with the duration
          ign <- ign[reaction=="Ignored" & shift(reaction,type="lead")=="Touchpoint"]      # only include the appropriate types of differences
          sugs <- merge(sugs,ign[,c("recordId","dur"),with=F],by="recordId",all.x=T)       # merge the processed ignored table to the full suggestions table
          sugs <- sugs[is.na(dur),dur:=engageWindow+1]                                     # if there is no subsequent intereaction after a suggestionthan set the duration to one more than the engageWindow
      }
      sugs[reaction!="Ignored",dur:=0]                                                     # if suggestions are not ignored then set the duration to 0

      sugs$ctr <- 1                                                                        # setup to count the number of times that suggestions are engaged or not
      sugs$include <- F                                                                    # finish that setup
      sugs[dur<=engageWindow,include:=T]                                                   # this is where the suggestions <= engageWindow are included
      sugs[,c("yes","total"):=list(sum(include),sum(ctr)),by=c("repId","accountId")]       # count the number of suggestions offered and accepted
      sugs[,probAccepted:=yes/total,by=c("repId","accountId")]                             # that ratio is the probability estimate                               
    
      sugs[,k:=paste(repId,"_",accountId,sep="")]                                          # prepare for the rep x account non-suggestion engagement estimates
      events[,k:=paste(repId,"_",accountId,sep="")]                                        # prepare for the rep x account non-suggestion engagement estimates

 ###### example of sugs at this point in code    
 #        recordId     type                                                   suggestion       date                                                 Title_vod__c repActionTypeId feedbackDate  reaction accountId repId dur
 #     1:        1 Call_vod                           a207764~cp9:VVVV~r2717~V~1:d:p1003 2017-02-20                             SUGGESTED ACTION: Detail  LYRICA               3   2017-02-22  Complete    207764  2717   0
 #     2:        2 Call_vod                              a207764~cp10:~r2717~V~1:d:p1003 2017-06-26                             SUGGESTED ACTION: Detail  LYRICA               3         <NA>   Ignored    207764  2717   1
 #     3:        3 Call_vod                     a209818~cp9:~r2717~V~1:d:p1003:mt1:m1213 2017-01-05 SUGGESTED ACTION: Detail LYR Lyrica DPN August 2015 POA v1.0               3         <NA>   Ignored    209818  2717   6
 #     4:        4 Call_vod                               a209818~cp9:~r2717~V~1:d:p1003 2017-02-01                             SUGGESTED ACTION: Detail  LYRICA               3   2017-03-25 Execution    209818  2717   0
 #     5:        5 Call_vod                              a209818~cp9:V~r2717~V~1:d:p1003 2017-02-15                             SUGGESTED ACTION: Detail  LYRICA               3   2017-02-16 Execution    209818  2717   0
 #    ---                                                                                                                                                                                                                  
 #618933:   618933 Call_vod          a300707~cp11:~r3683~V~1:d:p1001~2:d:p1003~3:d:p1014 2017-08-01            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    300707  3683  61
 #618934:   618934 Call_vod a47737~cp11:~r3683~V~1:d:p1001~2:d:p1003~3:d:p1009~4:d:p1014 2017-07-27   SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX, FLECTOR               3         <NA>   Ignored     47737  3683  61
 #618935:   618935 Call_vod         a189462~cp11:V~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-08-01            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    189462  3683  61
 #618936:   618936 Call_vod        a192615~cp11:SV~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-07-18            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    192615  3683  61
 #618937:   618937 Call_vod          a194741~cp11:~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-07-25            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    194741  3683  61
 ####### end of example
    
    acctReps <- unique(rbind(sugs[date>=sugStartDate,c("k","repId","accountId"),with=F], events[,c("k","repId","accountId"), with=F]) ) # pick out all unique account rep combinations
    
    setkey(events,k,date)                                                                # sort events by rep account date
    events[,diff:=c(NA,diff(date)),by=c("repId","accountId")]                            # find interevent time differences by rep account

    flog.info("Now prepare for calculation of likelihood in parallel, using %s cores", numberCores)

    M <- dim(acctReps)[1]                                                                # setup for parallel processing
    N <- data.table(rowIndex=1:M,group=1:numberCores)    # grouping for each core

    likelihood <- foreach(nc = 1:numberCores , .combine=rbind) %dopar%                   # loop for each core
        {
          inds <- N[group==nc]$rowIndex               # index of rows assigned to be run by this core 
          indsLen <- length(inds)
          
          likelihood <- data.table(repId=numeric(indsLen),accountId=numeric(indsLen),probTouch=numeric(indsLen))  # for collection of results
            
            for(i in 1:indsLen)
            {
              ind <- inds[i]  
              ydata <- events[k==acctReps[ind]$k]                                        # pick out data for a single rep-account combination

                if(nrow(ydata)>2)                                                        # if there are less than 3 data points then skip - not enough data for estimation
                {
                    prob <- 0
                    lastDiff <- today-ydata[,.SD[.N]]$date                               # find the time between the last interaction and today
                    maxDiff <- max(ydata$diff,na.rm=T)                                   # find the maximum time between interactions
                    if(lastDiff<=maxDiff)                                                # only continue if the last difference is less than the max
                    {
                        fn <- kcde(ydata[!is.na(diff)]$diff,h=1)                         # estimate the ECDF using the kcde() function with smoothing parameter h=1
                        prob <- (1 - E(fn,lastDiff)) - (1 - E(fn,lastDiff+lookForward))  # estimate the probability that an intereaction will happen between the the time of the last one and the lookForward days
                    }
                    loc <- i
                    set(likelihood,loc,1,acctReps[ind,"repId",with=F])                     # save the results in the likelihood table that was setup above
                    set(likelihood,loc,2,acctReps[ind,"accountId",with=F])
                    set(likelihood,loc,3,prob)
                }
            }
            return(likelihood)                                                           # return here is for the parallel processing loop
        }

    flog.info("Done with calculating probTouch using events/interactions. Now estimates the likelihood of an interaction by day of week and week of month.")

    dayEstimate <- interactions[repActionTypeId==chl & date<=today] 
    
    # estimates the likelihood of an interaction by day of week and week of month
    dayEstimate <- calculateDaysEstimate(dayEstimate, today, lookForward) 

    # add the probability that the suggestion was accepted estimated above to the likelihood estimates
    if (nrow(sugs) > 0) {
        likelihood <- merge(likelihood, unique(sugs[,c("repId","accountId","probAccepted"),with=F]), by=c("repId","accountId"), all=T) 
    } else {
        # non-trigger suggestion are empty; use likelihood and add default probAccepted
        likelihood$probAccepted <- 0.0
    }
    
    likelihood[is.na(probTouch),probTouch:=0]                                            # make sure missing estimates are zero
    likelihood[probTouch<epsilon,probTouch:=0]                                           # make small estimates zero

    likelihood <- likelihood[repId>0]                                                    

    # prepare to spread the estimates over the lookForward interval
    likelihood <- addWeekdayWeekmonth(likelihood, today, lookForward)

    if(nrow(likelihood) > 0) {
        likelihood <- merge(likelihood, dayEstimate, by=c("repId","accountId","mw.wd"), all.x=T)
    } else {
        # no data of likelihood. use dayEstimate and add default values
        likelihood <- dayEstimate
        likelihood$probTouch <- 0.0
        likelihood$probAccepted <- 0.0
        likelihood$date <- today + 1
    } 
    likelihood[is.na(ratio),ratio:=0]

    likelihood[, PT:=ratio*probTouch]                                                 # finialize the separate estimates of prob touch, prob accept, and the combination     

    likelihood[is.na(probAccepted), probAccepted:=0]
    likelihood[, PA:=probAccepted]
    
    likelihood[, PB:=(PA + PT*(1-PA))]

    flog.info("Get final likelihood.")
    
    tT <- copy(likelihood)                                                            # the next set of code is setup for saving results
    tT$suggestionType <- "Target"
    setnames(tT,"PT","probability")
    tT[,c("PA","PB"):=NULL]

    tA <- copy(likelihood)
    tA$suggestionType <- "Target"
    setnames(tA,"PA","probability")
    tA[,c("PT","PB"):=NULL]

    tB <- copy(likelihood)
    tB$suggestionType <- "Target"
    setnames(tB,"PB","probability")
    tB[,c("PA","PT"):=NULL]

 ##    likelihood <- unique(rbind(tT,tA,tB,fill=T))

    if (useForProbability == "T")
        likelihood <- unique(tT)
    else if (useForProbability == "A")
        likelihood <- unique(tA)
    else
        likelihood <- unique(tB)

    likelihood$repActionTypeId <- chl
    likelihood$runDate <- today+1

    likelihood[,c("learningRunUID","learningBuildUID","repUID","accountUID"):=list(RUN_UID,BUILD_UID,repId,accountId)]

    # return the results
    return(likelihood[,list(learningRunUID,learningBuildUID,repActionTypeId,repUID,accountUID,suggestionType,date,probability)])
}

