.sparkCombineCols <- function(sc, jdf, combinedCols, sep=NULL, newColName=NULL, keepOriCols=TRUE) {
  if (is.null(sep)) {sep <- ""}
  if (is.null(newColName)) {newColName <- "col"}
  # convert string to column object
  listOfCols <- lapply(combinedCols,sqlf(sc,"col"))
  allCols <- invoke(jdf,"columns")
  keptOriCols <- lapply(allCols[!allCols %in% combinedCols], sqlf(sc,"col"))
  # concat
  new_jdf <- jdf %>%
    invoke("withColumn", newColName, sqlf(sc, "concat_ws")("_",listOfCols))
  if (!keepOriCols) {
    new_jdf <- new_jdf %>%
      invoke("select", c(keptOriCols,list(sqlf(sc,"col")(newColName))))
  }
  # return
  return(new_jdf)
}

sparkCombineCols <- function(df, combinedCols, sep=NULL, newColName=NULL, keepOriCols=TRUE) {
  return(jdfFuncWrapper(.sparkCombineCols, df, combinedCols, sep, newColName, keepOriCols))
}