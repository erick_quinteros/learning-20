.sparkMelt <- function(sc, jdf, id_vars, value_vars = NULL, var_name = "variable", value_name = "value") {
  if (is.null(value_vars)) {
    allCols <- invoke(jdf, "columns")
    value_vars <- allCols[!allCols %in% id_vars]
  }
  # Combine columns into array<struct<key,value>> and explode
  exploded <- sqlf(sc, "explode")(sqlf(sc, "array")(lapply(value_vars, function(x) {
    key <- sqlf(sc, "lit")(x) %>% invoke("alias", var_name)
    value <- sqlf(sc, "col")(x) %>% invoke("alias", value_name)
    sqlf(sc, "struct")(list(key, value))
  })))
  # expand struct<..., struct<key, value>> into struct<..., key, value>
  exprs <- lapply(
    c(id_vars, paste("col", c(var_name, value_name), sep = ".")),
    sqlf(sc, "col"))
  # Explode and register as temp table
  new_jdf <- jdf %>%
    invoke("withColumn", "col", exploded) %>%
    invoke("select", exprs)
  # return
  return(new_jdf)
}

sparkMelt <- function(df, id_vars, value_vars = NULL, var_name = "variable", value_name = "value") {
  return(jdfFuncWrapper(.sparkMelt, df, id_vars, value_vars, var_name, value_name))
}