import pandas as pd

from common.pyUtils.database_utils import other_database_op
from common.pyUtils.database_config import DatabaseConfig
from common.pyUtils.logger import get_module_logger
from anchorAccuracy.utils.AnchorTableNamesConfig import AnchorTableNamesConfig

logger = get_module_logger(__name__)


class AccuracyReportSaver:
    """
    object to handle saving accuracy analysis result to DB
    """

    cols_write_to_db = ['repId', 'date', 'visitRadiusKm', 'predictedVisitedDiffKm', 'maxDayTravelKm', 'visitAreaKm2',
                        'overlapAreaKm2', 'numOfVisits', 'numOfVisitsInOverlap', 'numOfSugs', 'numOfSugsInPredict',
                        'numOfSugsTaken', 'numOfFacilityPredicted', 'numOfFacilityPredictedVisited',
                        'pctOverlapCoverage', 'pctVisitInOverlap', 'aggPctOverlapCoveragePctVisitInOverlap',
                        'pctSugsInPredict', 'pctSugsTaken', 'pctPredictedFacilityVisited',
                        'predictedVisitedDiffLessThanMaxTravel', 'hasVisited', 'hasPredicted', 'hasSuggested',
                        'maxDayTravelOverMaxNear', 'predictedVisitedMSE']
    accuracy_table_schema = "learning"
    accuracy_table_name = "RepDateLocationAccuracy"
    agg_accuracy_table_schema = "learning"
    agg_accuracy_table_name = "AggRepDateLocationAccuracy"

    @classmethod
    def __create_accuracy_table_for_paraRun(cls, self):
        class_accuracy_table_schema = self.db_config.schema_mapping[cls.accuracy_table_schema]
        if (self.accuracy_table_name != cls.accuracy_table_name) or (self.accuracy_table_schema != class_accuracy_table_schema):
            logger.info("check if accuracy table {}.{} exists".format(self.accuracy_table_schema, self.accuracy_table_name))
            # only create the table for paraRun (paraRun is not the default accuracy table)
            create_table_sql = '''CREATE TABLE IF NOT EXISTS {}.{} AS (SELECT * FROM {}.{} WHERE false);'''
            create_table_sql = create_table_sql.format(self.accuracy_table_schema, self.accuracy_table_name, class_accuracy_table_schema, cls.accuracy_table_name)
            other_database_op(create_table_sql, self.conn_pool)

    @classmethod
    def __create_agg_accuracy_table_for_paraRun(cls, self):
        class_agg_accuracy_table_schema = self.db_config.schema_mapping[cls.agg_accuracy_table_schema]
        if (self.agg_accuracy_table_name != cls.agg_accuracy_table_name) or (self.agg_accuracy_table_schema != class_agg_accuracy_table_schema):
            logger.info("check if agg accuracy table {}.{} exists".format(self.accuracy_table_schema, self.accuracy_table_name))
            # only create the table for paraRun (paraRun is not the default accuracy table)
            create_table_sql = '''CREATE TABLE IF NOT EXISTS {}.{} AS (SELECT * FROM {}.{} WHERE false);'''
            create_table_sql = create_table_sql.format(self.agg_accuracy_table_schema, self.agg_accuracy_table_name, class_agg_accuracy_table_schema, cls.agg_accuracy_table_name)
            other_database_op(create_table_sql, self.conn_pool)

    def __init__(self, conn_pool, start_date, end_date, is_nightly, accuracy_result, nightly_prediction_count=None ,run_uid=None, build_uid=None, version_uid=None):
        self.start_date = start_date
        self.end_date = end_date
        self.conn_pool = conn_pool
        self.is_nightly = is_nightly
        self.accuracy_result = accuracy_result
        # parameter needed for AggRepDateLocationAccuracy
        self.nightly_prediction_count = nightly_prediction_count
        self.run_uid = run_uid
        self.build_uid = build_uid
        self.version_uid = version_uid
        # update accuracy output write table
        table_config = AnchorTableNamesConfig.instance()
        self.accuracy_table_schema = table_config.RepDateLocationAccuracy['schema'] if table_config.RepDateLocationAccuracy is not None else self.accuracy_table_schema
        self.accuracy_table_name = table_config.RepDateLocationAccuracy['tableName'] if table_config.RepDateLocationAccuracy is not None else self.accuracy_table_name
        self.agg_accuracy_table_schema = table_config.RepDateLocationAccuracy_agg['schema'] if table_config.RepDateLocationAccuracy_agg is not None else None
        self.agg_accuracy_table_name = table_config.RepDateLocationAccuracy_agg['tableName'] if table_config.RepDateLocationAccuracy_agg is not None else None
        # update schema
        self.db_config = DatabaseConfig.instance()
        self.accuracy_table_schema = self.db_config.schema_mapping[self.accuracy_table_schema]
        self.agg_accuracy_table_schema = self.db_config.schema_mapping[self.agg_accuracy_table_schema]

    def processing(self):
        logger.info("Process Accuracy Result for saving")
        self.accuracy_result = self.accuracy_result.loc[:, self.cols_write_to_db]
        if self.is_nightly:
            self.accuracy_result["source"] = 'nightly'
            self.accuracy_result['learningBuildUID'] = self.build_uid
            self.accuracy_result['learningVersionUID'] = self.version_uid
        else:
            self.accuracy_result["source"] = 'history'

    def clean_DB(self):
        logger.info("Clean DB, prepare for save")
        clean_db_sql = None
        if self.is_nightly:
            clean_db_sql = '''DELETE FROM {}.{} WHERE date >= {!r} AND source='nightly';'''.format(self.accuracy_table_schema, self.accuracy_table_name, self.start_date)
        else:
            clean_db_sql = '''DELETE FROM {}.{} WHERE source = 'history';'''.format(self.accuracy_table_schema, self.accuracy_table_name)
        other_database_op(clean_db_sql, self.conn_pool)

    def save_to_DB(self):
        logger.info("save {} row of result to {}.{}".format(self.accuracy_result.shape[0], self.accuracy_table_schema, self.accuracy_table_name))
        receive_before_cursor_execute = self.conn_pool.customize_receive_before_cursor_execute()
        self.accuracy_result.to_sql(self.accuracy_table_name, con=self.conn_pool.get_engine(), schema=self.accuracy_table_schema, index=False, if_exists='append')

    def updateAggAccuracy(self):
        self.__create_agg_accuracy_table_for_paraRun(self)
        logger.info("Clean Agg Accuracy table, dropping row with version {}".format(self.version_uid))
        clean_agg_table_sql = '''DELETE FROM {}.{} WHERE learningVersionUID = '{}';'''.format(self.agg_accuracy_table_schema, self.agg_accuracy_table_name, self.version_uid)
        other_database_op(clean_agg_table_sql, self.conn_pool)
        logger.info("Computing Agg Accuracy metrics, for version {}".format(self.version_uid))
        update_agg_accuracy_sql = '''INSERT INTO {agg_table_schema}.{agg_table_name} (`learningVersionUID`,`latestLearningRunUID`, `pctOverlapCoverage`, `pctPredictedVisited`, `pctSuggestionPredicted`, `pctOverallPrediction`,`latestRunPredictionCount`)
            SELECT
            '{version_uid}',
            '{run_uid}',
            SUM(RDLA.`overlapAreaKm2`)/SUM(RDLA.`visitAreaKm2`) AS `pctOverlapCoverage`,
            SUM(RDLA.`NumOfVisitsInOverlap`)/SUM(RDLA.`NumOfVisits`) AS `pctPredictedVisited`,
            SUM(RDLA.`NumOfSugsInPredict` )/SUM(RDLA.`NumOfSugs`) AS `pctSuggestionPredicted`,
            (0.7 * (SUM(RDLA.`overlapAreaKm2`)/SUM(RDLA.`visitAreaKm2`))) + (0.3 * (SUM(RDLA.`NumOfVisitsInOverlap`)/SUM(RDLA.`NumOfVisits`))) AS `pctOverallPrediction`,
            {nightly_prediction_count} AS `latestRunPredictionCount`
            FROM  {table_schema}.{table_name} RDLA
            WHERE RDLA.`learningVersionUID` = '{version_uid}';'''.format(version_uid=self.version_uid, run_uid=self.run_uid, table_schema=self.accuracy_table_schema, nightly_prediction_count=self.nightly_prediction_count, table_name=self.accuracy_table_name, agg_table_name=self.agg_accuracy_table_name, agg_table_schema=self.agg_accuracy_table_schema)
        other_database_op(update_agg_accuracy_sql, self.conn_pool)

    def run(self):
        logger.info("Start saving accuracy analysis result to DB...")
        self.__create_accuracy_table_for_paraRun(self)

        if self.accuracy_result.shape[0]>0:
            self.processing()
            self.clean_DB()
            self.save_to_DB()
            if self.is_nightly and self.run_uid is not None and self.agg_accuracy_table_name is not None:
                self.updateAggAccuracy()
        else:
            logger.warn('Anchor accuracy result is empty!')
        logger.info("Finish saving accuracy analysis result to DB ")
