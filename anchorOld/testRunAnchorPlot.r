
##########################################################
#
# set up environment variables
#
##########################################################

library(futile.logger)

flog.threshold(INFO)

flog.info("Loading function library")
load("./library/AnchorLibrary.RData")

flog.info("BuildDate = %s",as.character(functionLibrary[["buildDate"]]))

load("./data/result.RData")

pdf("plotAnchor.pdf")
plotAnchor()
dev.off()
