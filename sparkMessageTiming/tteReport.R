##########################################################
##
##
## Aktana Machine Learning Module.
##
## Description: TTE Reporting Code
##   1. calculate best days to engage in hcp level 
##   2. calculate best days to engage in segment level 
##
## created by : wendong.zhu@aktana.com
##
## created on : 2018-08-15
##
## Copyright AKTANA (c) 2018.
##
##
##########################################################

DAYS_AFTER_SEND <- 1      # default number of days to visit after send
DAYS_AFTER_VISIT <- 1     # default number of days to send after visit
MAX_RECOMMEND_DAYS <- 60  # max days to recommend
DAYS_IN_WEEK <- 7

# TTE Reporting methods:
#   B - Covariates + Intervals
#   C - Covariates + DoW

ttePredictReport <- function(sc, con, con_l, dynamic.model, segs, models.B, models.C, AP, ints,
                             RUN_UID, whiteList, tteParams)
{
    library(h2o)
    library(data.table)
    library(sparkLearning)
    library(sparklyr)
    library(dplyr)

    flog.info("Entering ttePredictReport...")

    # retrieve tte parameters
    predictAhead <- tteParams[["predictAhead"]]
    LOOKBACK_MAX <- tteParams[["LOOKBACK_MAX"]]
    predictRunDate <- tteParams[["predictRunDate"]]
    segs <- tteParams[["segs"]]
    channels <- tteParams[["channels"]]
    VAL_NO_DATA <- tteParams[["VAL_NO_DATA"]]

    ints <- ints %>% collect() %>% data.table()
    
    setorder(ints, accountId, type, date)
    setorder(dynamic.model, accountId, event, date)
    
    ints.acct <- ints[, .(accountId)]
    ints.acct <- ints.acct[!duplicated(ints.acct[, .(accountId)])]

    ints.acct$ctr <- 1
    dup <- copy(ints.acct)

    dup <- rbind(dup, ints.acct[, ctr := 2])
    
    ints.acct <- dup
    
    ints.acct[ctr == 1, type := "SEND"]
    ints.acct[ctr == 2, type := "VISIT"]
    
    setorder(ints.acct, accountId, type)
    ints.acct$ctr <- NULL

    date.back <- predictRunDate - LOOKBACK_MAX - 1

    # pick the last row in each accountId group
    dm.s <- dynamic.model[event == "S", .SD[.N], by=accountId][, .(accountId, event, preS, preV, preT, pre2S, pre2V, numS, numV, date)] 
    dm.v <- dynamic.model[event == "V", .SD[.N], by=accountId][, .(accountId, event, preS, preV, preT, pre2S, pre2V, numS, numV, date)] 
    
    dm <- rbind(dm.s, dm.v)
    setorder(dm, accountId, event)
  
    dm <- dm[accountId %in% whiteList]  # filering by whiteList

    dm[event == "S", type := "SEND"]
    dm[event == "V", type := "VISIT"]
    dm$event <- NULL
    
    ints <- merge(dm, ints.acct, by=c("accountId", "type"), all = T)
  
    ints[is.na(preS), preS := LOOKBACK_MAX]
    ints[is.na(preV), preV := LOOKBACK_MAX]
    ints[is.na(preT), preT := VAL_NO_DATA]
    ints[is.na(numS), numS := 1]
    ints[is.na(numV), numV := 1]
    
    ints[is.na(pre2S), pre2S := VAL_NO_DATA]
    ints[is.na(pre2V), pre2V := VAL_NO_DATA]
    
    ints[is.na(date), date := date.back]

    ints[type == "SEND",  event := "S"]
    ints[type == "VISIT",  event := "V"]
    ints$type <- NULL
    ints$intervalOrDoW <- 1
    
    ints.bak <- copy(ints)
    dup  <- copy(ints)
    
    for(i in 1:(predictAhead-1)) {
      # Duplicate ints in order to predict future days
      # in each loop (one append), increase preS, preV, preT, etc. and date by 1
      if ("EVENT" %in% channels) {
        # handling event feature
        if ("preW" %in% names(ints) & ("preC" %in% names(ints))) {
          dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT", "preW", "preC", "intervalOrDoW") := 
                                   list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1, preW+1, preC+1, intervalOrDoW+1)])
        }
        else if ("preW" %in% names(ints)) {
          dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT", "preW", "intervalOrDoW") := 
                                   list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1, preW+1, intervalOrDoW+1)])
        }
        else if ("preC" %in% names(ints)) {
          dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT", "preC", "intervalOrDoW") := 
                                   list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1, preC+1, intervalOrDoW+1)])
        }
        else {
          dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT", "intervalOrDoW") := 
                                   list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1, intervalOrDoW+1)])
        }
      } else {
        dup <- rbind(dup, ints[, c("date", "preS", "preV", "pre2S", "pre2V", "preT", "intervalOrDoW") := 
                                 list(date+1, preS+1, preV+1, pre2S+1, pre2V+1, preT+1, intervalOrDoW+1)])
      }
    }
    
    setorder(dup, accountId, event, date)    
    method <- "B"
    
    ## calculate scores for reporting
    scores <- tteMethod(sc, models.B, AP, dup, RUN_UID, method)
    scores$method <- "Covariates + Intervals"

    rm(dup)
    gc()

    # now processing data for DoW method
    ints <- ints.bak[, c("accountId", "event", "date")]
    dup  <- copy(ints)
    
    for(i in 1:(DAYS_IN_WEEK - 1)) {
        # Duplicate ints in order to predict future 7 days
        # in each loop (one append), increase date by 1
        dup <- rbind(dup, ints[, c("date") := list(date+1)])
    }

    setorder(dup, accountId, event, date)    
    method <- "C"
    
    scores.C <- tteMethod(sc, models.C, AP, dup, RUN_UID, method)
    
    scores.C$method <- "Covariates + DoW"
    scores.C[, intervalOrDoW := as.POSIXlt(scores.C$date)$wday]  # 0, 1~6 for Sun, Mon~Sat
    scores.C <- scores.C[!intervalOrDoW %in% c(0,6)]
    
    scores <- rbind(scores, scores.C)

    scores[, date := as.Date(date)]
    
    scores <- sdf_copy_to(sc, scores, "scores", overwrite=TRUE, memory=FALSE)

    ## save reporting results
    saveTimingScoreReport(sc, con, con_l, scores, tteParams)
}
