import sys
import os
import multiprocessing as mp

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)

from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from simulationMessageSequence.SimulationMessageSequenceDriver import SimulationMessageSequenceDriver
from common.DataAccessLayer.DataAccessLayer import DSEAccessor
from common.DataAccessLayer.DataAccessLayer import DatabaseIntializer

from simulationMessageSequence.LearningPropertiesReader import LearningPropertiesReader
from simulationMessageSequence.LearningPropertiesKey import LearningPropertiesKey
import time

LEARNING_PROPERTIES_FILE_NAME = "learning.properties"
LEARNING_DB_SUFFIX = "_learning"
CS_DB_SUFFIX = "_cs"
PARALLEL_PROCESS_COUNT = 3

learning_home_dir = None


def get_mso_build_uid_list():
    """
    This function returns the list of MSO Build UIDs to process
    :return:
    """
    mso_build_uids = []
    dse_accessor = DSEAccessor()
    external_ids = dse_accessor.get_active_undeleted_message_algorithm_externalIds()
    for external_id in external_ids:
        print("Processing external Id: " + external_id)
        learning_prop_file_path = os.path.join(learning_home_dir, 'builds', 'nightly', external_id, LEARNING_PROPERTIES_FILE_NAME)
        learning_prop_config = read_learning_properties(learning_prop_file_path)
        mso_build_uid = learning_prop_config.get_property(LearningPropertiesKey.BUILD_UID)
        mso_build_uids.append(mso_build_uid)

    return mso_build_uids

def read_learning_properties(learning_prop_file_path):
    """
    This function reads learning properties from the specified build directory.
    :return:
    """
    assert os.path.isfile(learning_prop_file_path), "Learning Properties file does not exists"

    learning_prop_config = LearningPropertiesReader()
    learning_prop_config.read_learning_properties(learning_prop_file_path)

    return learning_prop_config

def get_copy_storm_database_name(dse_db_name):
    """
    This function returns the copy storm database name based on the logic and customer exceptions.
    :param dse_db_name:
    :return:
    """
    cs_db_name =  dse_db_name + CS_DB_SUFFIX

    if cs_db_name[:8] == "pfizerus":
        cs_db_name = 'pfizerprod_cs'

    return cs_db_name

def start_simulation(mso_build_uid):
    """
    This function loads the SimulationMessageSequenceDriver and start simulation for specified mso build
    :param mso_build_uid:
    :return:
    """
    global learning_home_dir

    simulation_seq_driver = SimulationMessageSequenceDriver(learning_home_dir, mso_build_uid)
    simulation_seq_driver.start_simulation()

def populate_database_tables():
    """
    This function loads data into the database `SimulationAccountSentEmail` and `ApprovedDocuments` tables
    :return: 
    """
    database_initializer = DatabaseIntializer()
    database_initializer.populate_email_data_all_products()
    database_initializer.populate_approved_documents_data_global()

def main():
    """
    This is main function for running simulation for message sequence in parallel
    :return: 
    """
    global learning_home_dir

    # Validate the input argument to the script
    if len(sys.argv) != 6:
        print("Invalid arguments to the simulation script")

    # Retrieve the arguments
    db_host = sys.argv[1]
    db_user = sys.argv[2]
    db_password = sys.argv[3]
    dse_db_name = sys.argv[4]
    db_port = 3306  # Update to 33066 for testing on Local machine
    learning_home_dir = sys.argv[5]

    # Create Learning database name
    learning_db_name = dse_db_name + LEARNING_DB_SUFFIX
    cs_db_name = get_copy_storm_database_name(dse_db_name)

    # Get the singleton instance of the database config and set the properties
    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name)

    mso_build_uids = get_mso_build_uid_list()

    # Start the data loading process in parallel
    """
    NOTE: Interaction data loading is moved to triggers instead. As the data loading is consuming more memory and
    time. 
    """
    # database_process = mp.Process(target=populate_database_tables)
    # database_process.start()

    # Use process pool to run the simulation process in parallel
    process_pool = mp.Pool(processes=PARALLEL_PROCESS_COUNT)
    results = process_pool.map(start_simulation, mso_build_uids)

    # database_process.join()


if __name__ == "__main__":
    main()

