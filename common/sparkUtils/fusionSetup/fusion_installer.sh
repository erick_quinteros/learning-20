#!/bin/bash


# ###################################################################################
# This is shell script to setup an AWS EC2 instance with all required system level  #
# packages, R, and Python Installation for Aktana learning module.                  #
# You can create an AMI (Amazon Machine Image) from the from the EC2 instance once  #
# the setup is complete and can be used invoke standalone machine or EMR cluster    #
# that can run Aktana learning module.                                              #
#                                                                                   #
#                   MODULE: Learning Fusion                                         #
#                   CREATED BY: Anwar Shaikh                                        #
#                   CREATED AT: 18-SEP-2019                                         #
#                   UPDATED BY: Anwar Shaikh                                        #
#                   UPDATED AT: 25-SEP-2019                                         #
#####################################################################################


# Updating system packages
sudo yum update -y

# Installing requried system packages
sudo yum install -y  bzip2-devel.x86_64 xz-devel.x86_64 pcre-devel.x86_64 ibxml2-devel texlive htop
sudo yum install -y xorg-x11-xauth.x86_64 xorg-x11-server-utils.x86_64 xterm libXt libX11-devel libXt-devel libcurl-devel git compat-gmp4 compat-libffi5
sudo yum install -y gcc gcc-c++ gcc-gfortran
sudo yum install -y readline-devel cairo-devel libpng-devel libjpeg-devel libtiff-devel openssl-devel libxml2-devel mysql-devel mesa-libGLU-devel.x86_64 unixodbc unixODBC-devel

# Create learning installation directory
INSTALL_DIR="/learning_fusion"
mkdir -p $INSTALL_DIR
cd $INSTALL_DIR

# Building and Installing  R
R_VERSION="R-3.5.2"
R_URL="http://cran.r-project.org/src/base/R-3/$R_VERSION.tar.gz"
pushd .
mkdir R-latest
cd R-latest
wget $R_URL
tar -xzf $R_VERSION.tar.gz
cd $R_VERSION
./configure --with-readline=yes --enable-R-profiling=no --enable-memory-profiling=no --enable-R-shlib --with-pic --prefix=/usr --with-x --with-libpng --with-jpeglib --with-cairo --enable-R-shlib --with-recommended-packages=yes
make -j 8
sudo make install
popd

# Perform R installation beforehand EMR initialization
# NOTE: If we don't install following packages while using this AMI on EMR. The
#       intialization process will install these packages and make R Default to
#       R version 3.4.2
sudo yum install -y R R-devel R-core R-core-devel R-java R-java-devel

# Rollback to default R 3.5
pushd .
cd $INSTALL_DIR/R-latest/$R_VERSION
make install
popd

# Set AWS Credentials
export AWS_ACCESS_KEY_ID="AKIAIULIODFOMLFJLOQA"
export AWS_SECRET_ACCESS_KEY="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"

# Get dependecy files from Git or S3
aws s3 cp s3://fusion-install-test/install/install_packages.r $INSTALL_DIR/install_packages.r
aws s3 cp s3://fusion-install-test/install/requirements.txt $INSTALL_DIR/requirements.txt

# Installing R Packages
sudo Rscript $INSTALL_DIR/install_packages.r


# Install Python 3.6
sudo yum -y install python36
sudo yum -y install python36-devel

# Installing Python Packages
sudo python3 -m pip install -r $INSTALL_DIR/requirements.txt
