##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: install Packages needed for spark modules
#
#
####################################################################################################################
# install devtools to manage package version
install.packages("devtools",repos="https://cran.rstudio.com/")
library(devtools)

# packaged needed for all modules for spark running
needed <- c("stringi","testthat")
install.packages(needed,repos="http://cran.rstudio.com")



# Packages needed for Fusion
needed <- c("futile.logger", "RMySQL", "ks","zoo", "tidyr", "anomalize", "config", "odbc", "data.table", "tidyverse", "dplyr")
inst <- installed.packages()
for(i in needed)
{
  if(!is.element(i,inst))
  {
    install.packages(i,repos="https://cran.rstudio.com/")
  }
}

# install specific version of data.table, h2o， reticulate
#install_version("data.table", version = "1.11.4", repos = "https://cran.rstudio.com")
