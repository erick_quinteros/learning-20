CREATE TABLE `EventType` (
  `eventTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `eventTypeName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `externalId` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `eventCategory` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `productIdRule` tinyint(1) DEFAULT '1' COMMENT '1=cannot be null, 0=must be null, null=optional',
  `messageTopicIdRule` tinyint(1) DEFAULT NULL COMMENT '1=cannot be null, 0=must be null, null=optional',
  `messageIdRule` tinyint(1) DEFAULT NULL COMMENT '1=cannot be null, 0=must be null, null=optional',
  `repIdRule` tinyint(1) DEFAULT NULL COMMENT '1=cannot be null, 0=must be null, null=optional',
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`eventTypeId`),
  UNIQUE KEY `eventType_uniqueExternalId` (`externalId`),
  UNIQUE KEY `eventType_uniqueActivityName` (`eventTypeName`)
) ENGINE=InnoDB AUTO_INCREMENT=1079 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci