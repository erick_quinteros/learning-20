CREATE TABLE `Account` (
  `accountId` int(11) NOT NULL AUTO_INCREMENT,
  `externalId` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `facilityId` int(11) DEFAULT NULL,
  `accountName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Soft delete flag.',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email_akt` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hasSharedTarget_akt` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discussedTopic_akt` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chxSampleEligibility_akt` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xelSegment_akt` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brandFlag_akt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `channelPreferencePos_akt` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `driverLyrica_akt` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barrierLyrica_akt` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `channelPreferenceNeg_akt` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bosSegment_akt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hasSharedTargetC2C3_akt` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hasSharedTargetC1C3_akt` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barrierLyricaTop1_akt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barrierLyricaTop2_akt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barrierLyricaTop3_akt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `driverLyricaTop1_akt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `driverLyricaTop2_akt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `driverLyricaTop3_akt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accessDesignation_akt` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sampleCommercialAccess_akt` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastSlideUsed_akt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enumHcpSpecialty_akt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `embedaBrandFlag_akt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duaveeNRxQuintile_akt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estringTRxQuintile_akt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `premarinOralNRxQuintile_akt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pvcTRxQuintile_akt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `whBrandPriorities_akt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opioidFlag_akt` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chxEucBalanceFlag_akt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enumBrandedPreference_akt` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enumChxZDCPHotList_akt` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enumDuloxetinePreference_akt` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enumLyrDosingCode_akt` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enumLyrInsightCode_akt` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enumLyrInsightSlideCode_akt` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LyrChannelFocus_akt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xtandiBrandPriority_akt` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`accountId`),
  UNIQUE KEY `account_uniqueExternalId` (`externalId`),
  KEY `account_facilityId` (`facilityId`)
) ENGINE=InnoDB AUTO_INCREMENT=26011354 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci