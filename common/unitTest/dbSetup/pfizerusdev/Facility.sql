CREATE TABLE `Facility` (
  `facilityId` int(11) NOT NULL AUTO_INCREMENT,
  `facilityName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `externalId` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `isoCountryCode` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ISO 2 or 3-char country code for the facility',
  `timeZoneId` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'IANA timeZone identifier for the facility',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `geoLocationString` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Format TBD',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Soft delete flag.',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`facilityId`),
  UNIQUE KEY `facility_uniqueExternalId` (`externalId`)
) ENGINE=InnoDB AUTO_INCREMENT=181250814 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci