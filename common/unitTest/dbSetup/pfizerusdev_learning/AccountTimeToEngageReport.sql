CREATE TABLE `AccountTimeToEngageReport` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `accountId` int(11) NOT NULL DEFAULT '0',
  `repActionTypeId` tinyint(4) NOT NULL DEFAULT '0',
  `intervalOrDoW` int(11) NOT NULL DEFAULT '1',
  `probability` double DEFAULT NULL,
  `runDate` date DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `method` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `tteAlgorithmName` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
);