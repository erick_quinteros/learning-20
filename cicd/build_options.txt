
# Mandatory Parameters

buildAgentLabel=rpython
buildContainerName=rpython
buildParams=R CMD build learningPackage && R CMD build sparkLearningPackage && R CMD build dataAccessLayer
appName=learning
appVersion=20.0
uploadTargetSnap=si-generic-snapshot-local
uploadTargetQa=si-generic-qa-local
uploadTargetRegression=si-generic-regression-local

##tarDir=
#tarGlob=
