##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: install needed packages for learning model
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2017-01-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
#
# install Packages needed for messageSequence code
#
##########################################################
args <- commandArgs(TRUE)

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the expressions.
if(length(args)==0){
  print("No arguments supplied.")
  # quit(save = "no", status = 1, runLast = FALSE)
}else{
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}

# install devtools to manage package version
install.packages("devtools",repos="https://cran.rstudio.com/")
library(devtools)

# packaged needed for all modules for spark running
needed <- c("dplyr","stringi","sparklyr","testthat")
install.packages(needed,repos="http://cran.rstudio.com")

needed <- c("futile.logger","RMySQL","Hmisc","openxlsx","RcppArmadillo","Rcpp","rgl","text2vec","uuid","properties","httr","RCurl","jsonlite","reticulate","fpc","jiebaR","openssl")
install.packages(needed,repos="http://cran.rstudio.com")

# install specific version of data.table, h2o
install_version("data.table", version = "1.11.4", repos = "http://cran.us.r-project.org")
install.packages("h2o", type="source", repos="http://h2o-release.s3.amazonaws.com/h2o/rel-yates/1/R") # 3.24.0.1

if (packageVersion("reticulate") != "1.10") {
  install_version("reticulate", version = "1.10", repos = "http://cran.rstudio.com")
}

# # install spark
# library(sparklyr)
# spark_install(version = "2.3.1")

#
# This code builds the learning package and then installs it
#
# 
# # R CMD build learningPackage && R CMD INSTALL Learning_1.0.tar.gz
# shellCode <- sprintf("tar -cvf Learning.tar.gz %s/learningPackage",homedir)
# system(shellCode)
# # if(is.element("Learning",inst))remove.packages("Learning")
# install.packages("Learning.tar.gz", repos = NULL, type = "source")
# 
# # R CMD build sparkLearningPackage && R CMD INSTALL sparkLearning_1.0.tar.gz 
# shellCode <- sprintf("tar -cvf sparkLearning.tar.gz %s/sparkLearningPackage",homedir)
# system(shellCode)
# # if(is.element("sparkLearning",inst))remove.packages("sparkLearning")
# install.packages("sparkLearning.tar.gz", repos = NULL, type = "source")

