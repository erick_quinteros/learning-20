
from pyspark.context import SparkContext
import string
from utils import Utils
from pyspark.sql.types import *
import logging

logger = None
DSERUN_TABLES = ["dserun","dserunrepdate","dserunrepdatesuggestion","dserunrepdatesuggestiondetail"]

class Data_loading:
    def __init__(self, glue_context, customer, environment, df_load_pandas,df_map_pandas, batch):
        self.glueContext = glue_context
        self.customer = customer
        self.environment = environment
        self.utils = Utils(glue_context, customer)
        self.spark = glue_context.spark_session
        self.df_map_pandas = df_map_pandas
        self.df_load_pandas = df_load_pandas
        self.batch = batch

        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")

    def sql_to_template(self, sqlcommand, values):
        '''
        Templated sqlstring to be replaced by the values
        :param sqlcommand:
        :param values:
        :return:
        '''

        template = string.Template('%s' % (sqlcommand))
        sqlstr = template.substitute(values);
        sqlstr = sqlstr.replace('\n', ' ');
        sqlstr = sqlstr.strip()
        return sqlstr




    def get_column_mapping(self, df_map_pandas, values_database):
        '''
        Reads mapping dictionary and populates the mapping values for customer
        :param df_map_pandas: mapping dictionary from column mapping file
        :param values_database: holds the mapping values for customer
        :param customer: actual customer from config file
        :return:  mapping values for customer
        '''
        for index, row in df_map_pandas.iterrows():
            if row['Customer_name'] == self.customer:
                if len(row['Masked_name']) != 0:
                    name_split = row['Customer_attr_watermark'].split(' ')
                    name_split.append(name_split.pop())
                    #print(row['Masked_name'], ' '.join(each for each in name_split))
                    values_database[row['Masked_name']] = ' '.join(each for each in name_split)

        return values_database


    def preparesql(self, df_load_pandas, values_database, tablename):
        '''

        :param date_time:
        :param region:
        :param customer:
        :param environment:
        :param glueContext:
        :param df_load_pandas:
        :param values_database:
        :param tablename:
        :return:
        '''
        partition_columns_list = []
        for index, row in df_load_pandas.iterrows():
            # print(row['table_name'],tablename)
            if row['table_name'] == tablename:
                delta_col = row['delta_column']
                partition_columns_list = row['parition_keys_columns'].split(',')
                logger.info("tablename is matched -->", tablename)
                values_database['otherfilter'] = row['full']
                logger.info("Below is the Sql Query for table Name", tablename)
                sql_query = self.sql_to_template(row['sql_query'], values_database)
                logger.info(sql_query, partition_columns_list)
                return sql_query, partition_columns_list


    # derive a strategy for empty tables from reporting, similar to null columns in the mapping
    def load_tables_from_s3(self,tables, source_s3_location, archive_folder):
        '''
        :param tables:
        :param source_s3_location:
        :param environment:
        :param archive_folder:
        :return:
        '''
        ##if table is empty. we need to have basic structure ready with empty tables]
        # strategy_tables=['strategytarget','targetsperiod','targetinglevel']
        # tables_not_present=['message_topic_email_learned']
        # preuat -pfizerus
        # uat lillyus
        # prod - novsrtisus
        tables_found = []
        tables_not_found = []
        # get tables in source s3
        logger.info("start getting table names from archive")
        tables_in_source = self.utils.getArchiveRdsTables(source_s3_location)
        logger.info("successfully got tables")
        for table in tables:
            path = None
            sparkPath = None
            # if table == 'suggestions':
            #     table = 'rpt_suggestion_delivered_stg'
            #     path = 's3://aktana-bdp-adl/'+self.customer+'/prod/suggestions/'
            if table in DSERUN_TABLES:
                sparkTable = 'spark' + table
                if self.batch:
                    if table in tables_in_source:
                        path = source_s3_location + table
                    if sparkTable in tables_in_source:
                        # if no dserun*, just use 'path' to read sparkdserun*
                        if not path:
                            path = source_s3_location + sparkTable
                        else:
                            sparkPath = source_s3_location + sparkTable
                else:
                    if sparkTable in tables_in_source:
                        path = source_s3_location + sparkTable
                    else:
                        path = source_s3_location + table
            else:
                path = source_s3_location + table

            logger.info(path)
            logger.info("Loading the table from Parquet")
            df = self.utils.data_load_parquet(self.glueContext, path)
            if sparkPath:
                sparkDF = self.utils.data_load_parquet(self.glueContext, sparkPath)
                if len(sparkDF.columns) != len(df.columns):
                    common_cols = list(set(sparkDF.columns).intersection(set(df.columns)))
                    df = df.select(common_cols)
                    sparkDF = sparkDF.select(common_cols)
                df = df.union(sparkDF)
            if df.rdd.isEmpty():
                logger.info([table, "Table does not exist"])
                tables_not_found.append(table)
            else:
                df = df.repartition(40)
                df.createOrReplaceTempView(table)
                logger.info([table, " view has been created"])
                tables_found.append(table)
        return tables_found, tables_not_found

    def run (self, source_s3_location, s3_destination,archive_folder, bronze_tables, tables):
        logger.info("The data loading has been started from S3")
        tables_found, tables_not_found = self.load_tables_from_s3(tables, source_s3_location, archive_folder)
        logger.info([tables_found, "These are the tables in memory."])
        logger.info([tables_not_found, "These are the tables not found in memory"])

        # validating the sql queries from data loading file and prep data to write them in bronze area.
        values_database = {}
        for tablename in bronze_tables:
            if (tablename == "strategy_target" and "strategytarget" in tables_not_found):
                field = [StructField("strategyTargetId", IntegerType(), True), \
                         StructField("targetsPeriodId", IntegerType(), True),
                         StructField("targetingLevelId", IntegerType(), True),
                         StructField("facilityId", StringType(), True),
                         StructField("accountGroupId", StringType(), True),
                         StructField("accountId", IntegerType(), True),
                         StructField("repTeamId", IntegerType(), True),
                         StructField("repId", IntegerType(), True),
                         StructField("productId", IntegerType(), True),
                         StructField("messageTopicId", StringType(), True),
                         StructField("messageId", StringType(), True),
                         StructField("interactionTypeId", IntegerType(), True),
                         StructField("productInteractionTypeId", IntegerType(), True),
                         StructField("target", IntegerType(), True),
                         StructField("targetMin", IntegerType(), True),
                         StructField("targetMax", IntegerType(), True),
                         StructField("visitActionOrderMin", IntegerType(), True),
                         StructField("relativeValue", IntegerType(), True),
                         StructField("createdAt", TimestampType(), True),
                         StructField("updatedAt", TimestampType(), True)]
                schema = StructType(field)
                df = self.spark.createDataFrame(self.spark.sparkContext.emptyRDD(), schema)
                logger.info(df.count())
                df.repartition(1).write.format("delta").save(s3_destination + "data/bronze/strategy_target/")
                logger.info("empty strategy target table was created")
            else:
                values_database = self.get_column_mapping(self.df_map_pandas, values_database)
                result_sql, partition_columns_list = self.preparesql(self.df_load_pandas, values_database, tablename)
                if partition_columns_list[0] !="":
                    self.utils.df_topartition_delta(self.spark.sql(result_sql), partition_columns_list,
                                           s3_destination + "data/bronze/" + tablename)
                else: self.utils.df_to_delta(self.spark.sql(result_sql), s3_destination + "data/bronze/" + tablename)

            # print(spark.sql(result_sql))





