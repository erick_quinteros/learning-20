import logging
import sys
import os


def create_logger(logger_name, log_file_path, log_level):

    logger = logging.getLogger(logger_name)
    std_handler = logging.StreamHandler(stream=sys.stdout)
    formatter = logging.Formatter('%(asctime)s %(name)-5s %(levelname)-5s %(message)s')
    std_handler.setFormatter(formatter)
    logger.addHandler(std_handler)

    log_dir = os.path.dirname(log_file_path)
    try:
        os.makedirs(log_dir)
        print("Directory ", log_dir, " Created ")
    except FileExistsError:
        print("Directory ", log_dir, " already exists")

    if os.path.isdir(log_dir):
        file_handler = logging.FileHandler(log_file_path)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    logger.setLevel(log_level)
    return logger


def copy_log_file_to_s3(log_file_local_path, log_file_s3_path):
    copy_command = "aws s3 cp " + log_file_local_path + ' ' + log_file_s3_path
    os.system(copy_command)