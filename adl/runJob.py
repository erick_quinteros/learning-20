import time
import os
import sys
import json
import mysql.connector
from mysql.connector import Error
from datetime import datetime
from boto3.session import Session

customer = sys.argv[1]
environment = sys.argv[2]
job_name = 'ADL_' + customer + '_' + environment
ACCESS_KEY = ''
SECRET_KEY = ''
awsRegion = ''
job_state = ''

if __name__ == "__main__":

    # get CustomerADLConfig data
    file_path = sys.argv[3] + '/learning/adl'
    os.chdir(file_path)
    with open('customer-metadata.json', 'r') as f:
        config = json.load(f)

    try:
        connection = mysql.connector.connect(host=config["host"], database=config["database"], user=config["username"],
                                             password=config["password"])
        if connection.is_connected():
            cursor = connection.cursor()
            query = "select b.`adlS3Location`, b.`awsRegion`, b.`awsAccessKey`, b.`awsSecretKey`, b.`rptS3Location`, b.`environment`, b.`iamRole`, b.`mappingLocation` from `Customer` a join `CustomerADLConfig` b on a.customerId = b.customerId where a.`customerName`='{}' and b.`environment`='{}'".format(
                customer, environment)
            cursor.execute(query)
            record = cursor.fetchall()
            if (len(record) == 1):
                awsRegion = record[0][1]
                ACCESS_KEY = record[0][2]
                SECRET_KEY = record[0][3]
                print('Success reading from CustomerADLConfig table')
            else:
                print('Customer Information not found in CustomerADLConfig table')
                exit(1)
    except Error as e:
        print("Error", e)
        exit(1)
    finally:
        # closing database connection.
        if (connection.is_connected()):
            cursor.close()
            connection.close()


    # Run the job and catch status
    try:
        session = Session(aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY, region_name=awsRegion)
        client = session.client('glue')
        response = client.start_job_run(JobName=job_name)
        if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
            job_run_id = response['JobRunId']
            status = client.get_job_run(JobName=job_name, RunId=job_run_id)
            if status:
                state = status['JobRun']['JobRunState']
                while state != 'SUCCEEDED':
                    status = client.get_job_run(JobName=job_name, RunId=job_run_id)
                    state = status['JobRun']['JobRunState']
                    time.sleep(15)
                    if state in ['SUCCEEDED', 'STOPPED', 'FAILED', 'TIMEOUT']:
                        job_state = state
                        break

            print("Job State: " + job_state)

            # Get logs from aws
            try:
                cloudwatch = session.client('logs')
                response = cloudwatch.get_log_events(logGroupName='/aws-glue/jobs/output', logStreamName=job_run_id)
                for x in response["events"]:
                    print(
                        datetime.utcfromtimestamp(x["timestamp"] / 1000).strftime('%Y-%m-%d %H:%M:%S') + ":" + x[
                            "message"])
            except Exception as e:
                print(e)
                exit(1)

            if job_state != 'SUCCEEDED':
                exit(1)

        else:
            print(response)
            exit(1)
    except Exception as e:
        print(e)
        exit(1)
