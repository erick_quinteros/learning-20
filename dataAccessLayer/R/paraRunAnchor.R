#########################################################
# update & create register output tables & archiving for parallel run
#########################################################
dataAccessLayer.anchor.paraRun.updateOutputTables <- function() {
  # register
  dataAccessLayer.anchor.paraRun.registerOutputTables()
  # create tables
  for (table in names(anchorOutputTables)) {
    eval(parse(text = sprintf("dataAccessLayer.anchor.paraRun.create.%s()", table)))
  }
}

dataAccessLayer.anchor.paraRun.archiveOutput <- function() {
  # register archive table list
  anchorTablesToArchive <- dataAccessLayer.anchor.paraRun.registerTablesToArchive()
  # create archive tables if not exists & archive
  for (table in anchorTablesToArchive) {
    dataAccessLayer.anchor.paraRun.createArchiveTableFor(table)
    dataAccessLayer.anchor.paraRun.archive(table)
  }
}


#########################################################
# anchor register output tables for parallel run, archiving
#########################################################
dataAccessLayer.anchor.paraRun.registerOutputTables <- function() {
  anchorOutputTables <<- list( RepDateLocation_nightly = list(schema="learning", tableName="paraRun_RepDateLocation_nightly_newCluster"),
                               RepDateLocation_manual = list(schema="learning", tableName="paraRun_RepDateLocation_manual_newCluster"),
                               RepDateFacility_nightly = list(schema="learning", tableName="paraRun_RepDateFacility_nightly_newCluster"),
                               RepDateFacility_manual = list(schema="learning", tableName="paraRun_RepDateFacility_manual_newCluster"),
                               AccountDateLocation_nightly = list(schema="learning", tableName="paraRun_AccountDateLocation_nightly_newCluster"),
                               AccountDateLocation_manual = list(schema="learning", tableName="paraRun_AccountDateLocation_manual_newCluster"),
                               AccountDateLocationScores = list(schema="learning", tableName="paraRun_AccountDateLocationScores_newCluster"),
                               RepCalendarAdherence = list(schema="learning", tableName="paraRun_RepCalendarAdherence_newCluster"),
                               RepAccountCalendarAdherence = list(schema="learning", tableName="paraRun_RepAccountCalendarAdherence_newCluster")
                              )
  return(anchorOutputTables)
}

dataAccessLayer.anchor.paraRun.registerTablesToArchive <- function() {
  anchorTablesToArchive <- list( "RepDateLocation_nightly", 
                                  "RepDateFacility_nightly"
                                )
  return(anchorTablesToArchive)
}

#########################################################
# anchor common functions
#########################################################
dataAccessLayer.anchor.paraRun.create <- function(tableNameKey, tableSchema) {
  schema <- dataAccessLayer.anchor.getSchemaFromTableMap(tableNameKey)
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap(tableNameKey)
  dataAccessLayer.common.create(schema, tableName, tableSchema)
}

dataAccessLayer.anchor.paraRun.createArchiveTableFor <- function(tableNameKey) {
  schema <- dataAccessLayer.anchor.getSchemaFromTableMap(tableNameKey)
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap(tableNameKey)
  dataAccessLayer.common.createArchiveTableFor(schema, tableName)
}

dataAccessLayer.anchor.paraRun.archive <- function(tableNameKey) {
  schema <- dataAccessLayer.anchor.getSchemaFromTableMap(tableNameKey)
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap(tableNameKey)
  dataAccessLayer.common.archive(schema, tableName, colAsArcDate="createdAt")
}

#########################################################
# create tables for parallel run anchor
#########################################################
dataAccessLayer.anchor.paraRun.create.RepDateLocation_nightly <- function() {
  tableSchema <- "
  `repId` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `maxNearDistance` double DEFAULT NULL,
  `maxFarDistance` double DEFAULT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(31) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`repId`,`date`)
  "
  dataAccessLayer.anchor.paraRun.create("RepDateLocation_nightly", tableSchema)
}

dataAccessLayer.anchor.paraRun.create.RepDateLocation_manual <- function() {
  tableSchema <- "
  `repId` int(11) NOT NULL,
  `date` date NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `probability` double DEFAULT NULL,
  `maxNearDistance` double DEFAULT NULL,
  `maxFarDistance` double DEFAULT NULL,
  `source` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `runDate` date NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`,`date`,`latitude`,`longitude`,`source`,`learningRunUID`),
  KEY `idx_repId_date` (`date`,`repId`),
  KEY `RepDateLocation_fk_1` (`learningRunUID`),
  KEY `RepDateLocation_fk_2` (`learningBuildUID`)
  "
  dataAccessLayer.anchor.paraRun.create("RepDateLocation_manual", tableSchema)
}

dataAccessLayer.anchor.paraRun.create.RepDateFacility_nightly <- function() {
  tableSchema <- "
  `repId` int(11) NOT NULL,
  `date` date NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `probability` double DEFAULT NULL,
  `facilityId` int(11) NOT NULL,
  `source` varchar(31) COLLATE utf8_unicode_ci DEFAULT NULL,
  `learningRunUID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`,`date`,`facilityId`)
  "
  dataAccessLayer.anchor.paraRun.create("RepDateFacility_nightly", tableSchema)
}

dataAccessLayer.anchor.paraRun.create.RepDateFacility_manual <- function() {
  tableSchema <- "
  `repId` int(11) NOT NULL,
  `date` date NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `probability` double DEFAULT NULL,
  `facilityId` int(11) NOT NULL,
  `accountId` int(11) NOT NULL DEFAULT '0',
  `source` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `runDate` date NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`,`date`,`facilityId`,`accountId`,`source`,`learningRunUID`),
  KEY `idx_repId_date` (`date`,`repId`),
  KEY `RepDateFacility_fk_1` (`learningRunUID`),
  KEY `RepDateFacility_fk_2` (`learningBuildUID`)
  "
  dataAccessLayer.anchor.paraRun.create("RepDateFacility_manual", tableSchema)
}

dataAccessLayer.anchor.paraRun.create.AccountDateLocation_nightly <- function() {
  tableSchema <- "
  `accountId` int(11) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `date` date NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`accountId`,`date`)
  "
  dataAccessLayer.anchor.paraRun.create("AccountDateLocation_nightly", tableSchema)
}

dataAccessLayer.anchor.paraRun.create.AccountDateLocation_manual <- function() {
  tableSchema <- "
  `accountId` int(11) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `date` date NOT NULL,
  `facilityDoWScore` double NOT NULL,
  `runDate` date NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`accountId`,`date`)
  "
  dataAccessLayer.anchor.paraRun.create("AccountDateLocation_manual", tableSchema)
}

dataAccessLayer.anchor.paraRun.create.AccountDateLocationScores <- function() {
  tableSchema <- "
  `accountId` int(11) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `dayOfWeek` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `periodOfDay` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `facilityDoWScore` double NOT NULL,
  `facilityPoDScore` double NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `runDate` date NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`accountId`,`facilityId`,`dayOfWeek`,`periodOfDay`)
  "
  dataAccessLayer.anchor.paraRun.create("AccountDateLocationScores", tableSchema)
}

dataAccessLayer.anchor.paraRun.create.RepCalendarAdherence <- function() {
  tableSchema <- "
  `repId` int(11) NOT NULL,
  `completeProb` double NOT NULL,
  `completeConfidence` double NOT NULL,
  `rescheduleProb` double NOT NULL,
  `rescheduleConfidence` double NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `runDate` date NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`)
  "
  dataAccessLayer.anchor.paraRun.create("RepCalendarAdherence", tableSchema)
}

dataAccessLayer.anchor.paraRun.create.RepAccountCalendarAdherence <- function() {
  tableSchema <- "
  `repId` int(11) NOT NULL,
  `accountId` int(11) NOT NULL,
  `completeProb` double NOT NULL,
  `completeConfidence` double NOT NULL,
  `rescheduleProb` double NOT NULL,
  `rescheduleConfidence` double NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `runDate` date NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`,`accountId`)
  "
  dataAccessLayer.anchor.paraRun.create("RepAccountCalendarAdherence", tableSchema)
}
