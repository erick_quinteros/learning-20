context('testing messageSequence() func in MSO module')
print(Sys.time())

# load packages and scripts needed for running tests
library(h2o)
library(uuid)
library(Learning)
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))

# set up unit test build folder and related configs
# get buildUID
BUILD_UID <- readModuleConfig(homedir, 'messageSequence','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'messageSequence', BUILD_UID, files_to_copy='learning.properties')

# start h2o
print("start h2o early")
tryCatch(suppressWarnings(h2o.init(nthreads=-1, max_mem_size="24g")), 
         error = function(e) {
           flog.error('Error in h2o.init()',name='error')
           quit(save = "no", status = 65, runLast = FALSE) # user-defined error code 65 for failure of h2o initialization
         })

# load input data from loadMessageSequenceData
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactions.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountProduct.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_products.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNames.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
accountProduct$LyrChannelFocus_akt <- gsub('[\n|\r]','',accountProduct$LyrChannelFocus_akt)  # remove special character not supported by spark

# initialize config
# parameter defaults (needed for func in learning package work)
DRIVERMODULE <- "messageSequenceDriver.r"
RUN_UID <- UUIDgenerate()
runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=NULL, createExcel=FALSE, createPlotDir=FALSE, createModelsaveDir=TRUE)
config <- initializeConfigurationNew(homedir, BUILD_UID)
modelSaveDir <- sprintf("%s/builds/%s/models_%s", homedir, BUILD_UID, BUILD_UID)

# run messageSequence
source(sprintf("%s/messageSequence/messageSequence.R",homedir))
targetNames <- c('a3RA00000001MtAMAU', 'a3RA0000000e0u5MAA', 'a3RA0000000e0wBMAQ', 'a3RA0000000e486MAA', 'a3RA0000000e47gMAA') # force only build model for these five message
result_new <- messageSequence(config, runSettings[["modelsaveDir"]], interactions, accountProduct, products, emailTopicNames, interactionsP, targetNames)

# test cases
test_that("result has the correct output", {
  expect_length(result_new, 5)
  expect_equal(dim(result_new[["models"]]),c(2,11))
  expect_equal(dim(result_new[["output"]]),c(602,3))
  expect_equal(dim(result_new[["APpredictors"]]),c(204,3))
  expect_equal(result_new[["pName"]],"CHANTIX")
  expect_length(result_new[["predictorNamesAPColMap"]], 533)
  load(sprintf('%s/messageSequence/tests/data/from_messageSequence_models.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_messageSequence_output.RData', homedir))
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_APpredictors.RData',homedir))
  load(sprintf('%s/messageSequence/tests/data/from_messageSequence_pName.RData', homedir))
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_predictorNamesAPColMap.RData',homedir))
  expect_equal(result_new[["models"]][,c("target","Total")], models[,c("target","Total")])
  expect_equal(result_new[["output"]]$names, output$names)
  expect_equal(result_new[["APpredictors"]], APpredictors)
  expect_equal(result_new[["pName"]], pName)
  expect_equal(result_new[["predictorNamesAPColMap"]][order(unlist(result_new[["predictorNamesAPColMap"]]))], predictorNamesAPColMap[order(unlist(predictorNamesAPColMap))])
})

# test file structure
test_that("build dir has correct struture", {
  expect_file_exists(sprintf('%s/builds/',homedir))
  expect_file_exists(sprintf('%s/builds/%s',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/learning.properties',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,BUILD_UID,paste("Build",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,BUILD_UID,paste("Build",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/models_%s',homedir,BUILD_UID,BUILD_UID))
  expect_num_of_file(sprintf('%s/builds/%s/models_%s',homedir,BUILD_UID,BUILD_UID),2)
})

# run test cases on log file contents
# read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
log_contents <- readLogFile(homedir,BUILD_UID,paste("Build",RUN_UID,sep="_"))

test_that("check log file general structure", {
  ind <- grep('Number of targets:',log_contents)
  expect_str_end_match(log_contents[ind],'5')
  expect_str_end_match(log_contents[ind+1],'CHANTIX')
})

test_that("check processing AccountProduct/Account predictors", {
  ind <- grep('Analyzing account product data',log_contents)
  expect_length(ind,1)
  expect_str_start_match(log_contents[ind+1],'151 of 204')  # number of predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique value
  expect_str_start_match(log_contents[ind+2],'1 of 21 numeric predictors (53 all predictors)')  # number of numeric predictors from AccountProduct/Account dropped before loop to build models as there are either less than 5 unique values or having <2 quantile >0
  expect_str_start_match(log_contents[ind+3],'0 of 52')  # number of remaining predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique values (2nd check after processing numeric and character variable separately
  expect_str_end_match(log_contents[ind+4],'52 to 533')  # check for dcast manupilation
  expect_str_end_match(log_contents[ind+5],'added 72 and becomes 605') # check EmailTopic Open Rates predictor added
})

test_that("check message a3RA00000001MtAMAU have the correct design matrix", {
  ind <- grep('OPEN___a3RA00000001MtAMAU',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'29')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'319')  # number of SendName records
  expect_str_end_match(log_contents[ind+4],'222') # Number of accounts considered for dynamic features
  expect_str_end_match(log_contents[ind+6],'(227,34)')  # dim of predictors from dynamic features to static features
  expect_str_end_match(log_contents[ind+7],'(315,638)')  # dimension of start design matrix
  expect_str_end_match(log_contents[ind+9],'20')  # number of positive target records
  expect_str_end_match(log_contents[ind+10],'(227,638)')  # dimension of design matrix with positive send records
  expect_str_start_match(log_contents[ind+11],'133 of 638')  # number of predictors from design matrix allModel(static + dynamic features) dropped in target loop as they have less than 2 unique values after subsetting with send and filling NA with 0
  expect_str_start_match(log_contents[ind+12],'3 of 505')  # number of remaining predictors in the design matrix dropped because of learning.properties config setting
  expect_str_end_match(log_contents[ind+13],'(227,502)')  # final design matrix dimension
})

test_that("check message a3RA0000000e0u5MAA would be skipped as no targetName records", {
  ind <- grep('OPEN___a3RA0000000e0u5MAA',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'0')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'4')  # number of SendName records
  expect_str_start_match(log_contents[ind+3],'Finish modeling, Nothing to model as there are no targets in the data.')
})

test_that("check message a3RA0000000e0wBMAQ would be skipped as less than 5 positive OPEN", {
  ind <- grep('OPEN___a3RA0000000e0wBMAQ',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'1')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'7')   # number of SendName records
  expect_str_end_match(log_contents[ind+4],'7') # Number of accounts considered for dynamic features
  expect_str_end_match(log_contents[ind+6],'(7,37)')  # dim of predictors from dynamic features to static features
  expect_str_end_match(log_contents[ind+7],'(309,641)')  # dimension of start design matrix
  expect_str_end_match(log_contents[ind+9],'1')  # number of positive target records
  expect_str_start_match(log_contents[ind+10],'Finish modeling, Nothing to model as there are not enough positive target records in design matrix to build model.')
})

test_that("check message a3RA0000000e486MAA have the correct design matrix", {
  ind <- grep('OPEN___a3RA0000000e486MAA',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'24')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'76')   # number of SendName records
  expect_str_end_match(log_contents[ind+4],'68') # Number of accounts considered for dynamic features
  expect_str_end_match(log_contents[ind+6],'(72,38)')  # dim of predictors from dynamic features to static features
  expect_str_end_match(log_contents[ind+7],'(313,642)')  # dimension of start design matrix
  expect_str_end_match(log_contents[ind+9],'21')  # number of positive target records
  expect_str_end_match(log_contents[ind+10],'(71,642)')  # dimension of design matrix with positive send records
  expect_str_start_match(log_contents[ind+11],'282 of 642')  # number of predictors from design matrix allModel(static + dynamic features) dropped in target loop as they have less than 2 unique values after subsetting with send and filling NA with 0
  expect_str_start_match(log_contents[ind+12],'16 of 360')  # number of remaining predictors in the design matrix dropped because of learning.properties config setting
  expect_str_end_match(log_contents[ind+13],'(71,344)')  # final design matrix dimension
})

test_that("check message a3RA0000000e47gMAA would be skipped as less than 5 positive OPEN", {
  ind <- grep('OPEN___a3RA0000000e47gMAA',log_contents)
  expect_length(ind,1)
  expect_str_end_match(log_contents[ind+1],'1')  # number of TargetName records
  expect_str_end_match(log_contents[ind+2],'14')   # number of SendName records
  expect_str_end_match(log_contents[ind+4],'14') # Number of accounts considered for dynamic features
  expect_str_end_match(log_contents[ind+6],'(14,36)')  # dim of predictors from dynamic features to static features
  expect_str_end_match(log_contents[ind+7],'(309,640)')  # dimension of start design matrix
  expect_str_end_match(log_contents[ind+9],'1')  # number of positive target records
  expect_str_start_match(log_contents[ind+10],'Finish modeling, Nothing to model as there are not enough positive target records in design matrix to build model.')
})

# final clean up
closeClient()