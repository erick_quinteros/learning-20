##########################################################
##
##
## aktana-learning Score code for message sequence model
##
##
## created by : marc.cohen@aktana.com
##
## created on : 2017-11-03
##
## Copyright AKTANA (c) 2015.
##
##
##########################################################
library(h2o)
library(lattice)
library(openxlsx)
library(data.table)
library(Hmisc)
library(Learning)

###########################
## Func: scoreModel.processInts
###########################
scoreModel.processInts <- function(whiteList, interactions) {
  ## filter interactions by whiteList
  if(length(whiteList)==0){whiteList <- unique(interactions$accountId)}  # if whitelist empty then use all accounts
  ints <- interactions[accountId %in% whiteList,]   # subset the interactions to the product and whitelist
  if(dim(ints)[1]==0)  { return(NULL) }
  ## processing ints
  setkey(ints,accountId,date)
  ints <- ints[productInteractionTypeName %in% c("RTE_CLICK","RTE_OPEN","SEND") & !is.na(physicalMessageUID)]   # include events with non-null message id
  rm(interactions,envir=parent.frame())                                                                         # clean up memory
  gc()                                                                                                          # collect garbage
  ints[productInteractionTypeName=="RTE_OPEN",key:=paste0("OPEN___",physicalMessageUID)]                        # create key with event and message id
  ints[productInteractionTypeName=="RTE_CLICK",key:=paste0("CLIC___",physicalMessageUID)]
  ints[productInteractionTypeName=="SEND",key:=paste0("SEND___",physicalMessageUID)]
  return(ints)
}

###########################
## Func: scoreModel.prepareDesignMatrix
###########################
scoreModel.prepareDesignMatrix <- function(AP, ints, interactionsP, scoreDate, DAYS_CONSIDER_FOR_PRIORVISIT) {
  ##### build dynamic features
  t <- ints[,c("key","accountId")]                             # subset the key and accountid columns in the interactions
  t$ctr <- 1                                           
  t <- as.data.table(dcast(t,accountId~key))                          # create and account x key matrix with the number of observations in the interaction table for the account x key combination
  m <- melt(t,id.vars="accountId")                                    # melt that count matrix in order to set all non-zero values to one
  m[value>0,value:=1]                                                 # done here
  t <- as.data.table(dcast(m,accountId~variable))                     # transpose back now with ones when an event for a message was executed
  allModel <- merge(t,AP,by="accountId",all=T)                        # merge into the accountProduct data using full outer join; subset later
  allModel[is.na(allModel)] <- 0                                      # replace NAs with 0
  allModel <- allModel[,sapply(allModel,function(x)length(unique(x))>1),with=F]            # pick out variable withs more than 1 unique value
  
  ####### add priorVisit features to allModel
  # add priorCall features
  flog.info("processing visit interactions")
  intsV <- interactionsP[productInteractionTypeName=="VISIT_DETAIL", c("accountId","date")]       # get Visit data from
  intsV <- intsV[intsV$date<=scoreDate & intsV$date >= scoreDate-DAYS_CONSIDER_FOR_PRIORVISIT] # further subset intsV by date
  intsV <- unique(intsV)
  intsV <- intsV[,.(date=max(date)),by=accountId] # get the latest visit date for each account
  setkey(intsV, accountId)
  calculateDaysFromPriorVisit <- function (accountIdG) { # fucntion for finding days between send the lastest visit
    lastestVisitDate <- intsV[intsV$accountId==accountIdG]$date
    priorVisit <- as.numeric(scoreDate-lastestVisitDate)
    return (priorVisit)
  }
  flog.info("Adding priorVisit feature from Visit Channel to allModel")
  allModel$priorVisit <- -1
  allModel[accountId %in% intsV$accountId, priorVisit:=mapply(calculateDaysFromPriorVisit,accountId)]
  # convert to binary
  encodePriorVisitMultiBinary <- function(priorVisitC) {
    priorVisitList <- rep(list(0), DAYS_CONSIDER_FOR_PRIORVISIT+1)
    if (priorVisitC >= 0 & priorVisitC <= DAYS_CONSIDER_FOR_PRIORVISIT) {
      priorVisitList[[priorVisitC+1]] <- 1
    }
    return (priorVisitList)
  }
  flog.info("Processing priorVisit feature from Visit Channel, make it into binary")
  allModel[,paste("priorVisit_", 0:DAYS_CONSIDER_FOR_PRIORVISIT, "_days_before_send" ,sep=""):=encodePriorVisitMultiBinary(priorVisit),by=1:nrow(allModel)]
  allModel$priorVisit <- NULL
  
  return(allModel)
}

###########################
## Func: scoreModel.getAcctsToReScore
###########################
scoreModel.getAcctsToReScore <- function(ints_acct_msg_ldate, intsSEND, messageRescoringTimes, msmMinCnt, rescoringLimit, percentageMessages, tgtName, msgId) {
  
  # for the target message, build a data.table -- for each account, how many messages (all kinds) received as of today the target message was delivered last time.
  # find out accts whether they get X number of all messages. First, subset using targetName
  accts_ldate_target <- ints_acct_msg_ldate[physicalMessageUID==tgtName,c("accountId","latestDate")] # each row is for each account
  
  if (dim(accts_ldate_target)[1]<=0) {
    flog.info("target message has not been delivered to any accounts, so nothing to rescore")
    accts_rescore <- numeric()
  } else {
    # append column latestDate to intsSend, in order to compare date
    ar <- merge(intsSEND, accts_ldate_target, by="accountId")
    # count messages
    ar_MsgCountAfterDeliver <- ar[date>latestDate, .(sum=.N), by="accountId"]
    
    # calculate rsTimes per account (sum buildUID, per message)
    rsTimes <- messageRescoringTimes[physicalMessageUID==tgtName, .(accountId,rescoringTimes)]
    
    # find the accounts need to be rescored
    # create a data.table contain: account ID, MsgCountAfterDeliver, rescoringTimes
    if (nrow(rsTimes) > 0) {
      ar_MsgCountAfterDeliver <- merge(ar_MsgCountAfterDeliver, rsTimes, by="accountId", all.x=TRUE, sort=FALSE) # left join
      # replace NA with 0
      ar_MsgCountAfterDeliver[is.na(ar_MsgCountAfterDeliver$rescoringTimes),rescoringTimes:=0]
    } else { # no account rescored for this message
      flog.info("No account rescored previously for the message %s", tgtName)
      ar_MsgCountAfterDeliver[, rescoringTimes:=0]
    }
    # filter the account based of rescoringLimit setting
    if (rescoringLimit != -1) {
      before_rescoringLimit_nrow <- nrow(ar_MsgCountAfterDeliver)
      ar_MsgCountAfterDeliver <- ar_MsgCountAfterDeliver[rescoringTimes<rescoringLimit]
      flog.info("RescorinTimes Limit is %s: %s/%s accounts rescoringTimes not hit this limit",rescoringLimit,before_rescoringLimit_nrow,nrow(ar_MsgCountAfterDeliver))
    } else {
      flog.info("No rescoringTimes Limit")
    }
    # filter based on which the account send out enough message to rescore
    # these accounts are retrieved from interactions; Note that accounts from AP (or allModel, t) are much more than those in interactions.
    # get accountIds that meet requirement of rescoring for the target message (received X number (wait4Nmsg) of ANY messages since latestDate)
    wait4Nmsg <- floor(msmMinCnt[messageId==msgId]$cnt * percentageMessages/100)
    accts_rescore <- ar_MsgCountAfterDeliver[sum >= wait4Nmsg]$accountId
  }
  
  if (length(accts_rescore) <= 0) {
    flog.info("no accounts to be rescored for message %s though rescoringFlag=TRUE.........", tgtName)
    accts_rescore <- NULL
  } else {
    print("Here are accounts which have received %s messages and to be rescored:", wait4Nmsg)
    print(accts_rescore)
    flog.info("There are %s of Accounts to be rescored, which already received %s messages.", length(accts_rescore), wait4Nmsg)
  }
  
  return(accts_rescore)
}

###########################
## Func: scoreModel.scoreMessage
###########################
scoreModel.scoreMessage <- function(t, targetName, models, msgId, needFurtherDcastInChunks=FALSE, colsForDcast=character()) {
  
  if(dim(t)[1]==0) {                                                 # no records to predict since all have either sent or received target message
    flog.info("No records to predict since all accounts have either sent or received target message.")
    return(NULL)
  }
  
  # do dcast for static design matrix part if needFurtherDcastInChunks
  if (needFurtherDcastInChunks) {
    # add static design matrix part
    colsForDcast <- colsForDcast[colsForDcast %in% names(t)]
    t[,newvalue:=1]
    colsNotDcast <- names(t)[!names(t) %in% c(colsForDcast,"newvalue")]
    t <- do.call(cbind, c(t[,..colsNotDcast], lapply(colsForDcast, function(x){eval(parse(text=sprintf("dcast(t, accountId~%s, value.var='newvalue', fill=0)[,-c('accountId')]", x)))})))               # do dcast for each column and then cbind
  }
  
  ######## processing the desing matrix for scoring (check variable, etc)
  # save accountIDs and drop in design matrix
  saveAccountId <- t$accountId                                        # save the accountIds but delete from the matrix used to predict
  t$accountId <- NULL
  
  # match predictors in allModels and the ones saved in model
  modelPath <- models[targetOri==targetName]$modelName                   # pluck the model name
  flog.info("Load model from directory %s",modelPath)
  rf.hex <- h2o.loadModel(path=modelPath)                             # load the model into h2o
  vars <- as.data.table(h2o.varimp(rf.hex)$variable)                  # get the variables used in the model build
  nt <- names(t)                                                      # this small section makes sure that the variables used to build the model and the current vars in the design are consistent
  nv <- vars$V1
  t <- t[,nt[nt %in% nv],with=F]
  addCols <- nv[!(nv %in% nt)]
  if (length(addCols)>0) { t[,(addCols):=0] }
  
  ########## predict to get scores
  flog.info("Predict model outcomes")
  nrow_t <- dim(t)[1]    # number of rows in design matrix
  
  # as.h2o(t) conversion takes long time for large object; now taking advantage of parallel processing of h2o.importFile
  # first need to use fast write fwrite to output design matrix to csv file.
  fwrite(t, "/tmp/t.csv", col.names=FALSE)
  # h2o.importFile is very fast due to parallel processing
  p.hex = h2o.importFile(path="/tmp/t.csv", destination_frame="p.hex", header=FALSE, col.names=names(t))
  flog.info("H2O object p.hex rows : %s", dim(p.hex)[1])
  # check h2o import to make sure the dimension matches
  nrow_p <- dim(p.hex)[1]
  nrow_empty <- nrow_p - nrow_t  # extra lines added by h2o.importFile in the front of p.hex
  if (nrow_empty > 0) {  # there are extra empty lines in the beginning of p.hex
    flog.info("slicing...")
    p.hex <- p.hex[(1+nrow_empty):nrow_p, ]  # remove empty lines in the beginning
  }
  flog.info("Removed empty lines if any. Check p.hex rows : %s", dim(p.hex)[1])
  
  # start predict (scoring) by calling h2o.predict
  tp <- h2o.predict(rf.hex, p.hex)                                    # run the h2o prediction function
  
  ############## processing and record scoring results
  tp <- as.data.table(tp)
  tp$AUC <-  as.numeric(min(h2o.auc(rf.hex),h2o.auc(rf.hex,xval=T)))  # capture the model AUC for weighting the prob estimates
  tp$accountId <- saveAccountId
  tp$messageName <- targetName
  tp$msgId <- msgId
  
  flog.info("Finish analyzing %s", targetName)
  
  ############### clean up h2o objects
  h2o.removeAll()
  rm(p.hex); rm(rf.hex); rm(t);
  gc(); gc(); gc();
  
  return (tp)
}

###########################
## Func: scoreModel.getScoresLowerUpperBound
###########################
scoreModel.getScoresLowerUpperBound <- function(scores, strategy) {
  if(dim(scores)[1]==0)                                                   # calculate bounds for different strategies for situations where there is no model but we want non-zero probs
  {
    lowerBound <- 0
    upperBound <- 1
  } else {
    tempP1 <- scores$p1 * scores$AUC  # create temporarily for random selection
    top <- max(tempP1)
    bottom <- min(tempP1)
    avg <- mean(tempP1)
    range1 <- top - avg
    range2 <- avg - bottom
    if(strategy == "Moderate"){
      lowerBound <- avg - (range2/2)
      upperBound <- avg + (range1/2)
    } else if(strategy == "Aggressive"){
      lowerBound <- avg 
      upperBound <- top  
    } else if(strategy == "Conservative"){
      lowerBound <- bottom
      upperBound <- avg
    } else {
      flog.warn("No messaging strategy specified. Using conservative strategy")
      lowerBound <- bottom
      upperBound <- avg
    }
  }
  return(list(lowerBound, upperBound))
}

###########################
## Main Function
###########################

scoreModel <- function(con_l, isNightly, flagRescoring, runDir, modelsaveDir, modelsaveSpreadsheetName, BUILD_UID, config, scoreDate, TargetNames, whiteList, products, accountProduct, interactions, interactionsP, messages, expiredMessages, emailTopicNames, msmMinCnt)
{
    flog.info("Score buildUID: %s",BUILD_UID)
  
    ## constant settings
    options("h2o.use.data.table" = TRUE)      # speed up h2o object conversion
    DAYS_CONSIDER_FOR_PRIORVISIT <- 30        # constant config for priorVisit
    
############################### check and prepare model save references ####################################
    # find the model save directory
    if(!dir.exists(modelsaveDir)) {
        flog.warn("Model Directory not found: %s",modelsaveDir);
        return(list(1,NULL,NULL))
    }
    # find the model save excel
    if(!file.exists(modelsaveSpreadsheetName)) {
        flog.warn("XLSX file not found: %s",modelsaveSpreadsheetName);
        return(list(1,NULL,NULL))
    }
    models <- data.table(read.xlsx(modelsaveSpreadsheetName,sheet=3))                         # models table contains the references to the models
    
################################ read learning.properties config ############################################
    prods <- getConfigurationValueNew(config, "LE_MS_addPredictorsFromAccountProduct")         # need parameters to replicate design used to build
    pName <- products$productName
    messageAnalysisAllMessages <- 0

    predictorNamesAPColMap <- list()
    if (length(prods)!=0) {
      # remove extra unnessary string <repAccountAttribute\:> added to learning.properties <LE_MS_addPredictorsFromAccountProduct> by DSE API & and save its original mapping for UI display
      predictorNamesAPColMap <- as.list(prods)
      prods <- gsub("repAccountAttribute\\:", "", prods, fixed=TRUE)
      predictorNamesAPColMap <- setNames(predictorNamesAPColMap, as.vector(prods))
      predictorNamesAPColMap <- list2env(predictorNamesAPColMap) # convert to hash for faster access
      
      # remove predictors not in AP
      prods <- prods[prods %in% colnames(accountProduct)]
    }
        
############################### build static design matrix #################################################
    ## this is to find out whether needs to remove /n in AP (for old models only)
    # isOldModel <- any(grepl("\n", read.xlsx(modelsaveSpreadsheetName,sheet=2)$namesOri))
    # flog.info("check whether it is old model which contains '\n' in its variable names: %s", isOldModel)
    
    flog.info("Processing static predictors")
    temp <- buildStaticDesignMatrix(prods, accountProduct, emailTopicNames, logDropProcess=TRUE)
    AP <- temp[["AP"]]
    APpredictors <- temp[["APpredictors"]]
    predictorNamesAPColMap <- temp[['predictorNamesAPColMap']]
    colsForDcast <- temp[["colsForDcast"]]
    needFurtherDcastInChunks <- temp[["needFurtherDcastInChunks"]]
    chunkSize <- temp[["chunkSize"]]
    rm(temp)
    gc();gc();gc()
    
############################### build design matrix (part common to all messages) #########################
    # process interactions based on whiteList
    ints <- scoreModel.processInts(whiteList, interactions)
    if(is.null(ints)) {  # woops - nothing to do so return with no result
        flog.warn("No accounts in whitelist shared with product interactions")
        return(list(1,NULL,NULL))
    }
    # add dynamic features, priorVisit and build design matrix
    allModel <- scoreModel.prepareDesignMatrix(AP, ints, interactionsP, scoreDate, DAYS_CONSIDER_FOR_PRIORVISIT)

######################## capture the allModel matrix for compare with previous one #############################
    amsCnt <- 0   # initialize cnt 

    if (isNightly) {
        fle <- sprintf("%s/Design_nightly.RData",runDir)
    } else {
        fle <- sprintf("%s/Design.RData",runDir)
        amsCnt <- dbGetQuery(con_l, "SELECT count(1) cnt from AccountMessageSequence")$cnt
    }

    if(file.exists(fle) & amsCnt > 0)                             # check if there is an AP saved
    {
        flog.info("Saved Design matrix exists.")
        load(fle)                                                 # load the last one saved
        if(sum(!(names(oldallModel) %in% names(allModel)))==0)                # bail out if names don't match
        {
            flog.info("Old and New Design matrix field names match")
            duptmp <- data.table(row=c(1:dim(oldallModel)[1],1:dim(allModel)[1]),flag=duplicated(rbind(oldallModel,allModel,fill=T)),name=c(rep("oldallModel",dim(oldallModel)[1]),rep("allModel",dim(allModel)[1])))                                   # build the compare table 
            allModel <- allModel[duptmp[flag==F & name=="allModel"]$row]            # build the new AP
            flog.info("Size of changed design records. Number of rows = %s",dim(allModel)[1])
            oldallModel <- rbind(allModel,oldallModel[!oldallModel$accountId %in% allModel$accountId],fill=T)  # add the oldAP to the new AP for saving
            rm(duptmp)
        } else { flog.info("Old and new Design matrix field names DISAGREE"); oldallModel <- allModel; }
    } else { flog.info("Saved Design matrix does not exist."); oldallModel <- allModel; }                                        # if none has been saved save it now
 
    if (!isNightly) { # only save for manual run, which means for nightly run, it always scores.
        flog.info("Save Design Matrix")
        save(oldallModel,file=fle)
    }

    rm(oldallModel)                                                           # done with the old one until next time
    if(dim(allModel)[1]==0)                                                   # nothing to score
    {
      flog.info("Nothing to score so returning")
      return(list(1,NULL,NULL))
    }

####################### rescoring & isNighlty configs and necessary configs preparations #####################
    if (flagRescoring & isNightly) {
      ## get flagRescoring & is Nightly configs
      # rescoring parameters
      rescoringType   <- getConfigurationValueNew(config,"LE_MS_rescoringType")
      rescoringPeriod <- getConfigurationValueNew(config,"LE_MS_rescoringPeriod",convertFunc=as.numeric)
      rescoringLimit  <- getConfigurationValueNew(config,"LE_MS_rescoringLimit",convertFunc=as.numeric)
      if (rescoringType == "Percentage") {
        percentageMessages <- rescoringPeriod
      } else {
        flog.warn("rescoringType %s is not supported. Set flagRescoring to FALSE.",rescoringType)
        flagRescoring <- FALSE
        break
      }
      
      ## find the latest date in a message sent to an account
      # get send interactions
      intsSEND <- unique(interactionsP[productInteractionTypeName=="SEND" & !is.na(physicalMessageUID)][,c("accountId","physicalMessageUID","date")])
      # find latest date in each group of accountId, message, SEND. 
      ints_acct_msg_ldate <- intsSEND[, .(latestDate=max(date)), by=.(accountId,physicalMessageUID)]
      
      # load required messageRescoringTimes to check for num of times an account has been rescored
      messageRescoringTimes <- data.table(dbGetQuery(con_l, sprintf("SELECT accountId, physicalMessageUID, messageId, learningBuildUID, rescoringTimes, createdAt, updatedAt FROM MessageRescoringTimes WHERE learningBuildUID='%s';",BUILD_UID)))
    }
    
#################################### start of score message loop ########################################
    flog.info("Analyzing product: %s",pName)
    targetNames <- models[!is.na(targetOri)]$targetOri                                       # targets that have models built

    scores <- data.table()
    messageRescoringTimesAdd <- data.table(accountId=character(), physicalMessageUID=character(), messageId=integer(), learningBuildUID=character(), rescoringTimes=integer())
    messageRescoringTimesNew <- data.table(accountId=character(), physicalMessageUID=character(), messageId=integer(), learningBuildUID=character(), rescoringTimes=integer(), createdAt=character(), updatedAt=character())

####### loop through each saved models (targatNames)    
    for(targetName in targetNames)                                      # loop for each target in Build Model workbook
    {
        flog.info("Analyze %s",targetName)
        tgtName <- gsub(substr(targetName,1,7),"",targetName) # take real message str
        
####### check whether score message or not (check expired & whether in messageSet needs to be scored)
        # check expire
        if (tgtName %in% expiredMessages) {
          flog.info ("targetName %s is expired, skip the scoring!",tgtName)
          next;
        }
        # check wehther in list of messages needs to scored
        if (isNightly) {
          # TargetName from processScores() are messages only with the messageSetId associated with messageAlgorithmId
          # if not in TargetName, just skip processing it.
          if(!tgtName %in% TargetNames) {
            flog.info("targetName is not in MessageSetMessages Target!")
            next;
          }
        }
        
####### find out messageId
        msgId <- unique(ints[physicalMessageUID == tgtName]$messageId)  # get the right messageId corresponding to physicalMessageUID from InteractionProduct
        if (length(msgId) == 0) {
          msgId <- messages[lastPhysicalMessageUID == tgtName]$messageId    # this tgt not existing in interactions, then use messageId from messages
        }
        if (length(msgId) >1) {
          msgId <- msgId[1]
          flog.info("Info: Multiple msgIds: %s", msgId)
        }
        flog.info("msgId is: %s", msgId)

######## find the accounts to be scored
        sendName <- paste0("SEND",substr(targetName,5,nchar(targetName)))   # the name of the send event for the target message
        t <- copy(allModel)                                                 # make a copy of full matrix since will subset but need to reuse for subsequence targets
        if(!sendName %in% names(t)) {
            t[,(sendName):=0]                      # no SEND the target message; add "send" column to design matrix and assign 0
        }
        if(!targetName %in% names(t)) {
            t[,(targetName):=0]                    # no OPEN/CLICK on the target message; add "target" column to design matrix and assign 0
        }
        flog.info("Before removing SENT+target and adding rescoring, Number of records to score: %s",dim(t)[1])
        
        acctsToScore <- t[t[[sendName]]==0 & t[[targetName]]==0, ][["accountId"]]
        
        # Rescoring the target message that has already been develiverred, if flag set to T.
        if (flagRescoring & isNightly) {
          accts_rescore <- scoreModel.getAcctsToReScore(ints_acct_msg_ldate, intsSEND, messageRescoringTimes, msmMinCnt, rescoringLimit, percentageMessages, tgtName, msgId)
          # not rescoring if accts_rescore is empty
          if (!is.null(accts_rescore)) {
            # add accouts to rescored to those that have never received or openned the target message, AND not in accts_rescore
            acctsToScore <- c(acctsToScore, accts_rescore)
            # recording rescoring message and accout to prepare to write to DB later
            rscr <- data.table(accountId=accts_rescore, physicalMessageUID=tgtName, messageId=msgId, learningBuildUID=BUILD_UID, rescoringTimes=1)
            messageRescoringTimesAdd <- rbind(messageRescoringTimesAdd, rscr, fill=T)
          }
        } else {  # no rescoring
            flog.info("Not rescore message %s... as flagRescoring=%s isNightly=%s", tgtName, flagRescoring, isNightly)
        }
        
############ score message
        t <- t[t$accountId %in% acctsToScore,]
        flog.info("Number of records to score: %s",dim(t)[1])
        
        # process t in chunks if needed
        if (needFurtherDcastInChunks) {
          nr <- nrow(t)
          ngroups <- ceiling(nr/chunkSize)
          t.list <- split(t, rep(1:ngroups, each=chunkSize, length.out=nr))
        } else {
          ngroups <- 1
          t.list <- list(t)
        }
        
        for (i in 1:ngroups) {
          t.grp <- t.list[[i]]
          tp <- scoreModel.scoreMessage(t.grp, targetName, models, msgId, needFurtherDcastInChunks, colsForDcast)
          if (!is.null(tp)) {
            scores <- rbind(scores,tp,fill=T)                                   # append the model predictions for this target to the other predictions
          }
          rm(tp)
          gc();gc();gc()
        }
        rm(t.list)
        rm(t)
        gc();gc();gc()
        
    }
    
############# finish loop for score saved models
    flog.info("Finish loop for scoring saved models")
    scored <- substr(unique(scores$messageName),8,100)
    targetType <- substr(targetNames[1],1,7)
    
########################################## scores for new messages #######################################

    strategy <- getConfigurationValueNew(config, "LE_MS_newMessageStrategy")
    flog.info("Messaging strategy: %s",strategy)
    
    tmp <- scoreModel.getScoresLowerUpperBound(scores, strategy)
    lowerBound <- tmp[[1]]
    upperBound <- tmp[[2]]

    newScores <- data.table()

    for(tgt in TargetNames)   # loop through all targets (MessageSet if ifNightly; all messages otherwise) to see which ones didn't have models so we need to sample to create probs
    {
      if(!tgt %in% gsub(targetType,"",models$targetOri))
      {
        flog.info("Random sample for message %s lower bound %s upper bound %s",tgt,lowerBound,upperBound)   # found one so use the lower and upper bounds on a uniform variate to get a prob
        if (tgt %in% expiredMessages) {
          flog.info ("message %s is expired, skip the random sampling and score!",tgt)
        } else {
          scr <- data.table(accountId=whiteList,p1=runif(length(whiteList),lowerBound,upperBound),AUC=1)
          msgId <- unique(ints[physicalMessageUID==tgt]$messageId)
          if (length(msgId) == 0) { # this tgt not existing in interactions, then use messageId from messages
            msgId <- messages[lastPhysicalMessageUID == tgt]$messageId
          }
          
          scr[, c("predict", "p0", "messageName", "msgId"):=list(1, 1-p1, paste0(targetType,tgt), msgId)]
          newScores <- rbind(newScores,scr,fill=T)
        }
      }
    }
    
##################################### final processing of the scores #########################################
    
    fullScores <- rbind(scores, newScores)
    if(dim(fullScores)[1]==0)
    {
        flog.info("Nothing scored so return")
        return(list(1,fullScores,NULL))
    }
    setkey(fullScores,accountId,messageName)
    setnames(fullScores,c("messageName","p1"),c("messageId","prob"))
    fullScores$p0 <- NULL
    fullScores[,messageId:=substr(messageId,8,nchar(messageId))]
    fullScores[,predict:=ifelse(predict==0,0,1)]
#    save(fullScores,file=sprintf("%s/%s_%s.RData",runDir,DRIVERMODULE,runStamp))  # save scores for debugging
    
    fullScores[,aveRate:=mean(predict),by="messageId"]                                                  # some cleanup of the table with results
    fullScores[predict==0,breakProb:=max(prob),by=c("messageId","predict")]
    fullScores[predict==1,breakProb:=min(prob),by=c("messageId","predict")]
    setkey(fullScores,messageId,aveRate,breakProb)
    
    # deal with messageRescoringTimes
    # check if there is any rescoring executed
    if (nrow(messageRescoringTimesAdd) > 0) {
      # add timestamp column to new rescoring counts
      nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
      messageRescoringTimesAdd$createdAt <- nowTime
      messageRescoringTimesAdd$updatedAt <- nowTime
      # combine new rescoring with existing ones with the same buildUIDs
      messageRescoringTimesNew <- rbind(messageRescoringTimesAdd, messageRescoringTimes)
      # sum up rescoring time (old+new)
      messageRescoringTimesNew <- messageRescoringTimesNew[, .(rescoringTimes=sum(rescoringTimes), createdAt=min(createdAt), updatedAt=max(updatedAt)), by=.(accountId,physicalMessageUID,messageId,learningBuildUID)]
    } else {
      flog.info("No rescoring executed for this nightly scoring job")
    }

    flog.info("Return from scoreModel")
    return(list(0,fullScores,messageRescoringTimesNew))                                                                          # if the first element in list returned is 1 then no scores - see cases above
}

