#!/bin/bash
#
# aktana-learning Install script for Aktana Learning Engines.
#
# description: Learning modules are written in R. The install script is used
#              to install the appropriate R packages required for the module
#
# created by : satya.dhanushkodi@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
MODULE=anchor
RSCRIPT=anchorDriver
PRG="$0"
LEARNING_HOME=`dirname "$PRG"`/..
export LEARNING_HOME
BIN_DIR=$LEARNING_HOME/bin
ENV_NAME="$1"

if [ -z "$1" ] ; then
   echo "Error: Specify Environment to run on";
   exit 1
fi

if [ $ENV_NAME == 'aktsunovionusuat' ]; then
  DB_HOST=aktanadevrds.aktana.com
  DB_NAME=sunovionusuat
  DB_USER=appadmin
  DB_PASSWORD=n7pPnHzmbBYCPCq
  RUN_DATE=2015-01-01
  CUSTOMER=sunovionus
fi


$BIN_DIR/runModule.sh -m $MODULE -r $RSCRIPT -h $DB_HOST -s $DB_NAME -u $DB_USER -p $DB_PASSWORD -d $RUN_DATE -c $CUSTOMER -e $ENV_NAME
