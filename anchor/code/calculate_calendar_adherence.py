import pandas as pd
import numpy as np


def count_complete_reschedule(callHistory):
    grouped = callHistory.groupby('interactionId')
    def count_completed_recheduled(group):
        repId = group.repId.values[0]
        accountId = group.accountId.values[0]
        completed_no_rescheduled = 0
        completed_rescheduled = 0
        not_completed_no_rescheduled = 0
        not_completed_rescheduled = 0
        valid_count = 1
        if len(group) == 1:
            if group.status.values == 'Submitted_vod':
                valid_count = 0
            else:
                not_completed_no_rescheduled = 1
        else:
            last_day_ind = np.argmax(group.LastModifiedDate.values)
            last_status = group.status.values[last_day_ind]
            callDatetime_nunique = group.callDatetime.nunique()
            if (last_status == 'Submitted_vod') & (callDatetime_nunique > 1):
                completed_rescheduled = 1
            elif (last_status == 'Submitted_vod') & (callDatetime_nunique <= 1):
                completed_no_rescheduled = 1
            elif callDatetime_nunique > 1:
                not_completed_rescheduled = 1
            else:
                not_completed_no_rescheduled = 1
        return pd.Series([repId, accountId, valid_count, completed_no_rescheduled, completed_rescheduled, not_completed_no_rescheduled, not_completed_rescheduled], index=['repId','accountId', 'valid_count', 'completed_no_rescheduled', 'completed_rescheduled', 'not_completed_no_rescheduled', 'not_completed_rescheduled'])
    calendarAdherenceCount = grouped.apply(count_completed_recheduled).reset_index()
    return calendarAdherenceCount


def agg_complete_reschedule_count(calendarAdherenceCount, agg_key):
    grouped = calendarAdherenceCount.groupby(agg_key)
    calendarAdherenceCount_agg = grouped[['valid_count', 'completed_no_rescheduled', 'completed_rescheduled', 'not_completed_no_rescheduled', 'not_completed_rescheduled']].sum().reset_index()
    calendarAdherenceCount_agg = calendarAdherenceCount_agg[calendarAdherenceCount_agg.valid_count>0]
    return calendarAdherenceCount_agg


class ConfidenceLevel:
    def __init__(self, alpha_level):
        self.alpha_level = alpha_level
        self.z_score = None
        self._populate_values()

    def _populate_values(self):
        if self.alpha_level == 0.01:
            self.z_score = 2.58
        elif self.alpha_level == 0.02:
            self.z_score = 2.33
        elif self.alpha_level == 0.05:
            self.z_score = 1.96
        else:
            raise NotImplementedError


def binomial_confidence_measure(confidence_level, total_number, success_number):
    z_score = ConfidenceLevel(confidence_level).z_score
    return 1 - (z_score * np.sqrt(
        (success_number / total_number) * (1 - (success_number / total_number)) / total_number))



def calculate_calendar_adherence(calendarAdherenceCount_agg, confidence_level):
    calendarAdherenceCount_agg['completed'] = calendarAdherenceCount_agg.completed_no_rescheduled + calendarAdherenceCount_agg.completed_rescheduled
    calendarAdherenceCount_agg['rescheduled'] = calendarAdherenceCount_agg.not_completed_rescheduled + calendarAdherenceCount_agg.completed_rescheduled
    calendarAdherenceCount_agg['completeProb'] = calendarAdherenceCount_agg.completed / calendarAdherenceCount_agg.valid_count
    calendarAdherenceCount_agg['rescheduleProb'] = calendarAdherenceCount_agg.rescheduled / calendarAdherenceCount_agg.valid_count
    calendarAdherenceCount_agg['completeConfidence'] = binomial_confidence_measure(confidence_level, calendarAdherenceCount_agg.valid_count, calendarAdherenceCount_agg.completed)
    calendarAdherenceCount_agg['rescheduleConfidence'] = binomial_confidence_measure(confidence_level, calendarAdherenceCount_agg.valid_count, calendarAdherenceCount_agg.rescheduled)
    calendarAdherenceCount_agg.drop(['valid_count', 'completed_no_rescheduled', 'completed_rescheduled', 'not_completed_no_rescheduled', 'not_completed_rescheduled', 'completed', 'rescheduled'], axis=1, inplace=True)
    return calendarAdherenceCount_agg
