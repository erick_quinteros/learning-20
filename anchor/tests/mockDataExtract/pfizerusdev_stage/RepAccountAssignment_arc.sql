SELECT * FROM `RepAccountAssignment_arc` WHERE `repId` IN (1605,1505,1001);

# rep 1505 does not have any accounts in RepAccountAssignment_arc. Used below query to find interactions for relevant dates and made up dummy data in RepAccountAssignment_arc for rep 1505. All accounts except 3357 and 172135 
# will be actively assigned to rep 1505
SELECT DISTINCT ia.accountId FROM InteractionAccount ia JOIN Interaction i ON ia.interactionId = i.interactionId AND repId = 1505 AND i.interactionTypeId = 4 AND i.startDateLocal >= "2016-9-21" AND i.startDateLocal <= "2017-11-10";

# tl;dr
# for rep 1605 make accounts 424250 and 51043 invalid
# for rep 1505 make accounts 3357 and 172135 and 163619 invalid 