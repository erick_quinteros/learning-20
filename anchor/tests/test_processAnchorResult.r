context('testing processAnchorResult() in ANCHOR module')
print(Sys.time())


# load library and source script
library(Learning)
library(properties)
library(data.table)
source(sprintf("%s/anchor/code/utils.r",homedir))
source(sprintf("%s/anchor/code/processAnchorResult.r",homedir))

# load required input parameter from learning.properties
buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
config <- read.properties(sprintf('%s/anchor/tests/data/%s/learning.properties',homedir,buildUID))
predictRundate <- getConfigurationValueNew(config,"LE_AN_manualPredictRundate", convertFunc=as.Date)

# load required data input to function
load(sprintf('%s/anchor/tests/data/from_calculateAnchor_result.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_calculateAnchorForNewRep_resultNewRep.RData', homedir))
result <- rbind(result,resultNewRep)

# run tests
test_that("test process in centroid centroid is the same as the one saved for manual run ", {
  # run processAnchorResult
  isNightly <- FALSE
  resultList_new <- processAnchorResult("Centroid", result, predictRundate, isNightly)
  result_centroid_new <- resultList_new[["resultCentroid"]]
  result_facility_new <- resultList_new[["resultFacility"]]
  
  # test cases
  expect_equal(dim(result_centroid_new), c(15,9))
  expect_equal(result_facility_new, NULL) # should be null when running in centroid mode
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  expect_equal(result_centroid_new, result_centroid)
})

test_that("test process in facility mode is the same as the one saved for manual run", {
  # run processAnchorResult
  isNightly <- FALSE
  resultList_new <- processAnchorResult("Facility", result, predictRundate, isNightly)
  result_centroid_new <- resultList_new[["resultCentroid"]]
  result_facility_new <- resultList_new[["resultFacility"]]
  
  # test cases
  expect_equal(dim(result_centroid_new), c(15,9))
  expect_equal(dim(result_facility_new), c(463,9))
  # result_centroid <- result_centroid_new
  # save(result_centroid, file=sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  # result_facility <- result_facility_new
  # save(result_facility, file=sprintf('%s/anchor/tests/data/from_processAnchorResult_facility.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_facility.RData', homedir))
  expect_equal(result_centroid_new, result_centroid)
  expect_equal(result_facility_new, result_facility)
})

test_that("test process in centroid centroid is the same as the one saved for nightly run ", {
  # run processAnchorResult
  isNightly <- TRUE
  resultList_new <- processAnchorResult("Centroid", result, predictRundate, isNightly)
  result_centroid_new <- resultList_new[["resultCentroid"]]
  result_facility_new <- resultList_new[["resultFacility"]]

  expect_equal(dim(result_centroid_new), c(15,9))
  expect_equal(result_facility_new, NULL) # should be null when running in centroid mode
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  expect_equal(result_centroid_new, result_centroid)
})

test_that("test process in facility mode is the same as the one saved for nightly run ", {
  # run processAnchorResult
  isNightly <- TRUE
  resultList_new <- processAnchorResult("Facility", result, predictRundate, isNightly)
  result_centroid_new <- resultList_new[["resultCentroid"]]
  result_facility_new <- resultList_new[["resultFacility"]]
  
  expect_equal(dim(result_centroid_new), c(15,9))
  expect_equal(dim(result_facility_new), c(391,9))
  # result_facility_DSE <- result_facility_new
  # save(result_facility_DSE, file=sprintf('%s/anchor/tests/data/from_processAnchorResult_facility_DSE.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_facility_DSE.RData', homedir))
  expect_equal(result_centroid_new, result_centroid)
  expect_equal(result_facility_new, result_facility_DSE)
})

