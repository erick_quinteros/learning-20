import os
import sys

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)
#import packages
from common.DataAccessLayer.DataAccessLayer import MessageAccessor
from common.DataAccessLayer.DataAccessLayer import SegmentAccessor
from common.DataAccessLayer.DataAccessLayer import DatabaseIntializer
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.DataAccessLayer.DataAccessLayer import LearningAccessor
from common.DataAccessLayer.DataAccessLayer import DSEAccessor
import itertools
import datetime
from common.logger.Logger import create_logger
import common.DataAccessLayer.DataAccessLayer as data_access_layer
import logging
import pandas as pd
import time
import requests
import json



class UpdateFacilityDriver:
    def __init__(self, learning_home_dir):
        self.learning_home_dir = learning_home_dir
        self.__initialize_logger()

    def __initialize_logger(self):
        """

        :return:
        """
        # Create logger path
        timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        log_file_path = self.learning_home_dir + "/../logs/UpdateFacilityDriver." + timestamp + ".stdout"

        # Crete logger with file and output
        global logger
        logger_name = "UpdateFacilityDriver"
        logger = create_logger(logger_name, log_file_path, logging.DEBUG)

        # Initialize data access layer logger
        data_access_layer.initialize_logger(logger_name)

    def startUpdate(self):
        dse_accessor = DSEAccessor()
        start = time.time()
        dse_accessor.update_facility_table()
        logger.info("Facility table updated")
        print("Time used:")
        print(time.time() - start)



def main():
    """
    This is main function
    """
    # Validate the input argument to the script

    # Retrieve the arguments
    input_args_dict = dict(i.split("=") for i in sys.argv[1:])
    try:
        db_host = input_args_dict['dbhost']
        db_user = input_args_dict['dbuser']
        db_password = input_args_dict['dbpassword']
        dse_db_name = input_args_dict['dbname']
        db_port = input_args_dict['port']  # Update to 33066 for testing on Local machine
        # db_port = 33066
        customer_name = input_args_dict['customer']

        # run_date = input_args_dict['rundate']
        learning_home_dir = input_args_dict['homedir']
        cs_db_name = input_args_dict['dbname_cs']
        learning_db_name = input_args_dict['dbname_learning']
    except KeyError as e:
        print("Could not get parameters:{}".format(e))
        return

    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name)

    updatefacilitydriver = UpdateFacilityDriver(learning_home_dir)
    updatefacilitydriver.startUpdate()




if __name__ == "__main__":
    main()
